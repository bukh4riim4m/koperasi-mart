<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datatransaksippobs extends Model
{
  protected $fillable = [
      'id','user_id','no_anggota','no_trx','nomorhp','status','sn','paket','tgl_trx','mutasi','keterangan','nominal','nta','status','saldo','aktif','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];
}
