<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Simpananadmin extends Model
{
  protected $fillable = [
      'id','name','no_anggota','tgl_setor','jth_tempo','no_trx','jenis_simpanan','mutasi','nominal','saldo','awal','ket','aktif','petugas','created_at','updated_at'
  ];

  public function jenisSimpanan(){
    return $this->belongsTo('App\JenisSimpanan','jenis_simpanan');
  }
  public function user_id(){
    return $this->hasOne('App\User','id');
  }
}
