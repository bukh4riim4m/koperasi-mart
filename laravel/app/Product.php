<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = [
    'id','operator','description','code','price','status','provider_sub','jual'
  ];
  protected $hidden = [
      'created_at','updated_at'//,'code','price','status','provider_sub','untung'
  ];
}
