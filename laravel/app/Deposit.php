<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
  protected $fillable = [
      'id','user_id','no_anggota','no_trx','tgl_trx','bank','nominal','status','aktif','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];
  public function statusId(){
    return $this->belongsTo('App\Statusdeposit','status');
  }
}
