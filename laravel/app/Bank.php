<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = [
      'id','logo','bank','no_rekening','atas_nama','aktif','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];
}
