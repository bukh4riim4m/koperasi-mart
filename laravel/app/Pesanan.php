<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
  protected $fillable = [
    'id','user_id','no_anggota','alasan','no_pemesanan','ongkir','berat','tanggal','kode','toko_id','total_harga','modal','kode_unik','bank_id','jumlah','propinsi','city','melalui','alamat','status','aktif','created_at','created_by','updated_at','updated_by','deteted_at','deleted_by'
  ];

  public function tokoId(){
    return $this->belongsTo('App\Toko','toko_id');
  }
  public function statusId(){
    return $this->belongsTo('App\Status','status');
  }

}
