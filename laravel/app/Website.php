<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
  protected $fillable = [
      'id','name','logo','url','token','created_at','updated_at'
  ];
}
