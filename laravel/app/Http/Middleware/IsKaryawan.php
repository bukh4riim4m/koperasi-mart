<?php

namespace App\Http\Middleware;

use Closure;

class IsKaryawan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && $request->user()->type == 'karyawan' && $request->user()->aktif=='1'){
            return $next($request);
        }
        $request->session()->invalidate();
        return redirect()->guest('/');
    }
}
