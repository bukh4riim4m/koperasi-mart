<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dashboard ="dashboard";
        if (Auth::user()->type =='admin') {
          return view('administrator.index',compact('dashboard'));
        }elseif (Auth::user()->type =='anggota') {
          return view('anggota.index',compact('dashboard'));
        }elseif (Auth::user()->type =='karyawan') {
          return view('karyawan.index',compact('dashboard'));
        }else {
          return view('pengurus.index',compact('dashboard'));
        }

    }
}
