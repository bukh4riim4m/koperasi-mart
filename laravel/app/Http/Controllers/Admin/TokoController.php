<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Toko;
use App\City;
use App\Province;
use App\Pesanan;
use App\Menutransaksi;
use App\Bukusaldotransaksi;
use App\Status;
use Excel;
use DB;
use Log;
use Image;

class TokoController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }

  public function produk(Request $request){
    $dashboard ="tokoOnline";
    $kode = "";
    $name = "";
    if ($request->action == 'tambah') {
      $this->validate($request, [
        'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
      // $foto = date('YmdHis').'.'.$request->gambar->getClientOriginalExtension();
      DB::beginTransaction();
      try {
        $image = $request->file('gambar');
        $imageName = $image->getClientOriginalName();
        $fileName = date('YmdHis')."_".$imageName;
        $directory = public_path('/gambars/');
        // return $directory;
        $imageUrl = $directory.$fileName;
        Image::make($image)->resize(500, 500)->save($imageUrl);

        $create = Toko::create([
          'kode'=> $request->kode,
          'name'=>$request->name,
          'harga'=>$request->harga,
          'aktif'=>1,
          'modal'=>$request->modal,
          'berat'=>$request->berat,
          'gambar'=>$fileName,
          'keterangan'=>$request->keterangan,
          'admin'=>$request->user()->id,
          'stok'=>$request->stok,
          'created_by'=>$request->user()->name
        ]);
      } catch (\Exception $e) {
        Log::info('Gagal tambah produk:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Upload Produk.','INFO');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Berhasil Upload Produk.','INFO');
      return redirect()->back();
    }elseif ($request->action == 'cari') {
      $kode = $request->kode;
      $name = $request->name;
    }elseif ($request->action == 'edit') {
      if ($request->gambar) {
        $this->validate($request, [
          'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
      }
      DB::beginTransaction();
      try {
        $edit = Toko::find($request->ids);
        $edit->name = $request->name;
        $edit->kode = $request->kode;
        $edit->harga = $request->harga;
        $edit->modal = $request->modal;
        $edit->berat= $request->berat;
        $edit->stok = $request->stok;
        $edit->keterangan = $request->keterangan;
        if ($request->gambar) {
          $destination_foto =public_path('/gambars/'.$edit->gambar);
          if(file_exists($destination_foto)){
                      unlink($destination_foto);
          }
          $image = $request->file('gambar');
          $imageName = $image->getClientOriginalName();
          $fileName = date('YmdHis')."_".$imageName;
          $directory = public_path('/gambars/');
          // return $directory;
          $imageUrl = $directory.$fileName;
          Image::make($image)->resize(500, 500)->save($imageUrl);
          $edit->gambar = $fileName;
        }
        $edit->update();
      } catch (\Exception $e) {
        Log::info('Gagal edit produk:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Upload Produk.','INFO');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Berhasil edit Produk.','INFO');
      return redirect()->back();
    }elseif ($request->action == 'hapus') {
      $hapus = Toko::find($request->ids);
      $hapus->aktif = 0;
      if ($hapus->update()) {
        flash()->overlay('Berhasil Hapus.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Gagal Hapus.','INFO');
      return redirect()->back();
    }
    $produks = Toko::where('aktif',1)->where('kode','LIKE','%'.$kode.'%')->where('name','LIKE','%'.$name.'%')->orderBy('id','DESC')->get();
    return view('administrator.toko.produk',compact('dashboard','produks','kode','name'));
  }
  public function belanja(Request $request){
    $dashboard ="transaksi";
    $kode = "";
    $name = "";
    if ($request->action == 'cari') {
      $kode = $request->kode;
      $name = $request->name;
    }elseif ($request->action == 'beli') {
      $produk = Toko::find($request->ids);
      // return dd($produk);
      //PROPINSI
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "key: da08d414a043e0001fd33b5b3ad29ad0"
        ),
      ));

      $response = curl_exec($curl);
      // return dd($response);
      $resarr =json_decode($response,true);
      $detail =  $resarr['rajaongkir']['results'];
      // return dd($detail);
      // $err = curl_error($curl);
      //
      // curl_close($curl);


      return view('administrator.toko.pemesanan', compact('produk','dashboard','kode','name','detail'));
    }
    $produks = Toko::where('aktif',1)->where('kode','LIKE','%'.$kode.'%')->where('name','LIKE','%'.$name.'%')->orderBy('id','DESC')->get();
    return view('administrator.toko.belanja',compact('dashboard','produks','kode','name'));
  }
  public function jumlah(Request $request){
    if($request->ajax()){
        Log::info('Jumlah = '.$request->jumlah);
        $hargas = Toko::find($request->ids);
        $total_harga = $hargas->harga * $request->jumlah;
        Log::info('Data harga : '.$hargas->harga);
        $label ="";
        $label.="<input type='hidden' name='total_harga' value='".$total_harga."'/>";
        $label.="<label>Rp. ".number_format($hargas->harga * $request->jumlah).",-</label>";
        Log::info('TOTAL harga : '.$total_harga);
    		return $label;
    	}
  }
  public function province(Request $request)
    {
    	if($request->ajax()){
        Log::info('ID province = '.$request->province);
        $citises = City::where('province_id',$request->province)->get();
        Log::info('$detail = '.$citises);
        $option = "";
        foreach ($citises as $key => $citys) {
          if ($key==0) {
            $option.="<option value=''>Pilih Kota</option>";
          }else {
            $option.="<option value='".$citys->city_id."'>".$citys->city_name."</option>";
          }

        }
    		return $option;
    	}
    }
    public function city(Request $request)
      {
      	if($request->ajax()){
          Log::info('ID province = '.$request->city);
          $hargas = City::where('city_id',$request->city)->first();
          Log::info('alamat = '.$hargas);
          $total = $request['berat'] * $request['jumlah'];
          $kg = $total/1000;
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=106&destination=".$hargas->city_id."&weight=".$total."&courier=jne",
            CURLOPT_HTTPHEADER => array(
              "content-type: application/x-www-form-urlencoded",
              "key:da08d414a043e0001fd33b5b3ad29ad0"
            ),
          ));
          $response = curl_exec($curl);
          Log::info('$response = '.$response);
          $resarr =json_decode($response,true);
          $jne =json_decode($response,true);
          $label ="";
          $label.="<input type='hidden' name='province' value='".$resarr['rajaongkir']['destination_details']['province']."'/>";
          $label.="<input type='hidden' name='city' value='".$resarr['rajaongkir']['destination_details']['city_name']."'/>";
          $label.="<input type='hidden' name='total_berat' value='".$total."'/>";
          $label.="<input type='hidden' name='ongkir' value='".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value']."'/>";
          $label.= "<p>".$resarr['rajaongkir']['results'][0]['name']."</p>";
          $label.= "<p>Total Berat : ".$total." Gram / ".$kg." Kg</p>";
          $label.= "<p>Ongkos Kirim Rp. ".number_format($resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'])."</p>";
          $label.= "<p>Estimasi : ".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd']." Hari </p>";
          return $label;
      	}
      }

      public function kirim(Request $request)
        {
        	if($request->ajax()){
            Log::info('ID province = '.$request->city);
            $hargas = City::where('city_id',$request->city)->first();
            Log::info('alamat = '.$hargas);
            $total = $request['berat'] * $request['jumlah'];
            $kg = $total/1000;
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "origin=106&destination=".$hargas->city_id."&weight=".$total."&courier=".$request['kirim'],
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key:da08d414a043e0001fd33b5b3ad29ad0"
              ),
            ));
            $response = curl_exec($curl);
            Log::info('$response = '.$response);
            $resarr =json_decode($response,true);
            $jne =json_decode($response,true);
            $label ="";
            $label.="<input type='hidden' name='province' value='".$resarr['rajaongkir']['destination_details']['province']."'/>";
            $label.="<input type='hidden' name='city' value='".$resarr['rajaongkir']['destination_details']['city_name']."'/>";
            $label.="<input type='hidden' name='total_berat' value='".$total."'/>";
            $label.="<input type='hidden' name='ongkir' value='".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value']."'/>";
            $label.= "<p>".$resarr['rajaongkir']['results'][0]['name']."</p>";
            $label.= "<p>Total Berat : ".$total." Gram / ".$kg." Kg</p>";
            $label.= "<p>Ongkos Kirim Rp. ".number_format($resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'])."</p>";
            $label.= "<p>Estimasi : ".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd']." Hari </p>";
            return $label;

        	}
        }
      public function bayar(Request $request){
        $dashboard = 'transaksi';
        if ($request->ids =="" || $request->berat =="" || $request->ongkir =="" || $request->jumlah =="" || $request->province =="" || $request->city =="" || $request->kirim =="" || $request->alamat =="") {
          flash()->overlay('Data Harus Lengkap.','INFO');
          return redirect()->back();
        }
      //   $this->validate($request, [
      //       'ids' => 'required',
      //       'berat' => 'required',
      //       'ongkir' => 'required',
      //       'jumlah' => 'required',
      //       'city' => 'required',
      //       'kirim' => 'required',
      //       'ongkir' => 'required',
      //       'alamat' => 'required',
      //       'province' => 'required'
      // ]);
        $kode = Toko::find($request->ids);
        $no_pesanan = date('YmdHis').$request->user()->id;
        $totalharga = $request->total_harga + $request->ongkir;
        DB::beginTransaction();
        try {
          Pesanan::create([
            'user_id'=>$request->user()->id,
            'no_anggota'=> $request->user()->no_anggota,
            'no_pemesanan'=> $no_pesanan,
            'tanggal'=> date("Y-m-d"),
            'kode'=>$kode->kode,
            'toko_id'=>$request->ids,
            'total_harga'=>$totalharga,
            'berat'=>$request->berat,
            'ongkir'=>$request->ongkir,
            'jumlah'=>$request->jumlah,
            'propinsi'=>$request->province,
            'city'=>$request->city,
            'melalui'=>$request->kirim,
            'alamat'=>$request->alamat.', '.$request->city.', '.$request->province,
            'status'=>1,
            'aktif'=>1,
            'created_by'=>$request->user()->id
          ]);
        } catch (\Exception $e) {
          Log::info('Gagal Transaksi:'.$e->getMessage());
          DB::rollback();
          flash()->overlay('Gagal Transaksi.','INFO');
          return redirect()->back();
        }
        DB::commit();
        $produk = Pesanan::where('no_pemesanan',$no_pesanan)->first();
        return view('administrator.toko.pembayaran', compact('dashboard','produk'));
      }
      public function permintaan(Request $request){
        $dashboard = 'pemesanan';
        if ($request->ids) {
          DB::beginTransaction();
          try {
            $datas = Pesanan::where('status',2)->where('id',$request->ids)->first();
            $datas->status = 3;
            $datas->update();
          } catch (\Exception $e) {
            Log::info('Gagal Transaksi:'.$e->getMessage());
            DB::rollback();
            flash()->overlay('Transaksi Gagal, Silahkan di ulangi kembali','INFO');
            return redirect()->back();
          }
          DB::commit();
          flash()->overlay('Transaksi berhasil di proses','INFO');
          return redirect()->back();
        }
        $datas = Pesanan::where('status',2)->get();
        return view('administrator.toko.permintaan_barang',compact('dashboard','datas'));
      }
      public function pembatalan(Request $request){
        $dashboard = 'pemesanan';
        if ($request->alasan) {
          DB::beginTransaction();
          try {
            $datas = Pesanan::where('status',2)->where('id',$request->ids)->first();
            $datas->status = 5;
            $datas->alasan = $request->alasan;

            $stok = Toko::find($datas->toko_id);
            $stok->stok = $stok->stok + $datas->jumlah;

            $saldoakhir = User::find($datas->user_id);
            $kembali = $saldoakhir->saldotransaksi + $datas->total_harga;
            $saldo = User::find($datas->user_id)->update([
              'saldotransaksi'=> $kembali
            ]);
            $bukusaldo = Bukusaldotransaksi::create([
              'user_id'=>$saldoakhir->id,
              'no_anggota'=>$saldoakhir->no_anggota,
              'no_trx'=>date('ymdhis'),
              'tgl_trx'=>date('Y-m-d'),
              'nominal'=>$datas->total_harga,
              'saldo'=>$kembali,
              'mutasi'=>'Kredit',
              'keterangan'=>'Pembatalan barang kode : '.$datas->kode.' No.trx :'.$datas->no_pemesanan,
              'aktif'=>1,
              'created_by'=>$request->user()->id,
            ]);
            $stok->update();
            $datas->update();
          } catch (\Exception $e) {
            Log::info('Gagal Transaksi:'.$e->getMessage());
            DB::rollback();
            flash()->overlay('Transaksi Gagal dibatalkan, Silahkan di ulangi kembali','INFO');
            return redirect()->back();
          }
          DB::commit();
          flash()->overlay('Transaksi berhasil dibatalkan','INFO');
          return redirect()->back();
        }
        $datas = Pesanan::where('status',2)->get();
        return view('administrator.toko.permintaan_barang',compact('dashboard','datas'));
      }
      public function kirimbarang(Request $request){
        $dashboard = 'pemesanan';
        if ($request->ids) {
          DB::beginTransaction();
          try {
            $datas = Pesanan::where('status',3)->where('id',$request->ids)->first();
            $datas->status = 4;
            $datas->update();
          } catch (\Exception $e) {
            Log::info('Gagal Transaksi:'.$e->getMessage());
            DB::rollback();
            flash()->overlay('Transaksi Gagal, Silahkan di ulangi kembali','INFO');
            return redirect()->back();
          }
          DB::commit();
          flash()->overlay('Transaksi berhasil di proses','INFO');
          return redirect()->back();
        }
        $datas = Pesanan::where('status',3)->get();
        return view('administrator.toko.barang_kirim',compact('dashboard','datas'));
      }
      public function terimabarang(Request $request){
        $dashboard = 'laporan';
        $stts = $request->status;
        $today = date('Y-m-d');
        $from = $today;
        $until= $today;
        if ($request->from && $request->until) {
          $from = date('Y-m-d', strtotime($request->from));
          $until= date('Y-m-d', strtotime($request->until));
        }

        $datas = Pesanan::whereBetWeen('tanggal',[$from,$until])->where('status','LIKE','%'.$request->status.'%')->whereBetween('status',[4,5])->get();
        if ($request->download==1) {
          $totalQuery = count($datas);
          $while = ceil($totalQuery / 500);
          $collections = collect($datas);
          return Excel::create($from.'-'.$until, function ($excel) use ($while, $collections) {
              for ($i = 1; $i <= $while; $i++) {
                  $items = $collections->forPage($i, 500);
                  $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                      $sheet->loadView('administrator.report._report_penjualan_barang', ['datas' => $items]);
                  });
              }
          })->export('xls');
        }
        $from = date('d-m-Y', strtotime($from));
        $until= date('d-m-Y', strtotime($until));
        $statuses = Status::whereBetween('id',[4,5])->get();
        return view('administrator.toko.laporan_barang',compact('dashboard','datas','stts','from','until','statuses'));
      }
      public function transaksi(Request $request){
        $dashboard = 'transaksi';
        //LOGIN
        // $data = array("email" => '212martcilegon@gmail.com', "password" => '96312345');
        // $data_string = json_encode($data);
        //
        // $ch = curl_init('http://serbamurah.online/api/v1/user/signin');
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //     'Content-Type: application/json',
        //     'Content-Length: ' . strlen($data_string))
        // );
        //
        // $result = curl_exec($ch);
        // $hasil = json_decode($result,true);
        // return $hasil;
        // Log::info('RESPON JAVAH2h:'.$result);
        //ENDLOGIN
        $menus = Menutransaksi::where('aktif',1)->orderBy('id','ASC')->get();
        return view('administrator.toko.transaksi',compact('dashboard','menus'));
      }
      public function pulsa(Request $request){
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif',1)->orderBy('id','ASC')->get();
        return view('administrator.toko.pulsa',compact('dashboard','menus'));
      }
      public function paketdata(Request $request){
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif',1)->orderBy('id','ASC')->get();
        return view('administrator.toko.paket_data',compact('dashboard','menus'));
      }
      public function voucher(Request $request){
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif',1)->orderBy('id','ASC')->get();
        return view('administrator.toko.voucher_listrik',compact('dashboard','menus'));
      }
      public function saldoojek(Request $request){
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif',1)->orderBy('id','ASC')->get();
        return view('administrator.toko.saldo_ojek',compact('dashboard','menus'));
      }
      public function paketsms(Request $request){
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif',1)->orderBy('id','ASC')->get();
        return view('administrator.toko.paket_sms',compact('dashboard','menus'));
      }
      public function transferpulsa(Request $request){
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif',1)->orderBy('id','ASC')->get();
        return view('administrator.toko.transfer_pulsa',compact('dashboard','menus'));
      }

}
