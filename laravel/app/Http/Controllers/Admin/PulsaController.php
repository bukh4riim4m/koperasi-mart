<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Product;
use App\Simpananadmin;
use App\Datatransaksippobs;
use App\Website;
use Excel;
use Log;

class PulsaController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function cekharga(Request $request)
    {
        $dashboard ="tokoOnline";
        $access_token = Website::find(1);
        $url = $access_token->url.'hargaagent/PLN';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url, [
                        'headers'=>[
                            'Accept' => 'application/json',
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer '.$access_token->token
                        ]
                    ]);
        if ($res->getStatusCode() == 200) {
          $pln =  json_decode($res->getBody()->getContents(), true);
        }
        foreach ($pln as $value) {
            if ($produks = Product::where('code', $value['code'])->first()) {
                if ($produks->jual < $value['price']) {
                  $produks->jual = $value['price']+300;
                }
                $produks->price = $value['price'];
                $produks->status = $value['status'];
                $produks->update();
            }
            // $produks = Product::create([
        //   'code'=>$value['code'],
        //   'description'=>$value['description'],
        //   'operator'=>$value['operator'],
        //   'price'=>$value['price'],
        //   'jual'=>$value['price'],
        //   'status'=>$value['status'],
        //   'provider_sub'=>$value['provider_sub']
        // ]);
        }
        $stts = $request->status;
        $deskropsi = $request->description;
        $providers = $request->provider;
        $nominal= str_replace(".", "", $request->jual);
        if ($request->action =='edit') {
            $edits = Product::find($request->ids)->update([
          'jual'=>$nominal
        ]);
            if ($edits) {
                flash()->overlay('Berhasil Diedit.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Gagal Diedit.', 'INFO');
            return redirect()->back();
        }
        $pulsa = Product::orderBy('id', 'ASC')->where('operator', 'LIKE', '%'.$providers.'%')->where('description', 'LIKE', '%'.$deskropsi.'%')->where('status', 'LIKE', '%'.$stts.'%')->get();

        return view('administrator.agen.data_harga_pulsa', compact('dashboard', 'pulsa', 'stts', 'deskropsi', 'providers'));
    }

    public function update()
    {
        $produks = Simpananadmin::where('aktif', 1)->where('awal', 1)->get();
        $year = date('Y');
        foreach ($produks as $value) {
            // if ($produkss = Simpananadmin::where('tgl_setor','2017-01-20')->where('aktif',1)->first()) {
            // $cari = User::where('no_anggota',$value->no_anggota)->first();
            $update = Simpananadmin::find($value->id);

            $update->jth_tempo = '2017'.date('-m-d', strtotime($update->tgl_setor));
            $update->update();
            flash()->overlay('BERHASIL.', 'INFO');
            // }
        }
    }
    public function selectprovider(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->provider);
            $pakets = Product::where('operator', $request->provider)->orderBy('jual', 'ASC')->get();
            Log::info('$detail = '.$pakets);
            $option = "";
            $option.="<option value=''>Pilih Nominal Pulsa</option>";
            foreach ($pakets as $key => $paket) {
                $option.="<option value='".$paket->code."'>".$paket->description."</option>";
            }
            return $option;
        }
    }
    public function selectpaket(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->paket);
            $pakets = Product::where('code', $request->paket)->first();
            Log::info('$detail = '.$pakets);
            $option = "";
            $option.="<label>Rp. ".number_format($pakets->jual).",-</label>";
            return $option;
        }
    }
    public function datappob(Request $request)
    {
        $dashboard = 'transaksi';
        $datas = Datatransaksippobs::where('no_anggota', $request->user()->no_anggota)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('administrator.agen.dataPpob', compact('dashboard', 'datas'));
    }

    public function pascabayar(Request $request)
    {
        flash()->overlay('PLN PASCABAYAR Belum dapat berfungsi.', 'INFO');
        return redirect()->route('admin-transaksi-pulsa');
    }
    public function reportppob(Request $request)
    {
        $dashboard = 'laporan';
        $from = date('Y-m-d');
        $until = $from;
        $stts = $request->status;
        if ($request->has('from') && $request->has('until')) {
          $from = date('Y-m-d',strtotime($request->from));
          $until = date('Y-m-d',strtotime($request->until));
        }
        $datas = Datatransaksippobs::whereBetWeen('tgl_trx',[$from,$until])->where('status','LIKE','%'.$stts.'%')->where('aktif', 1)->orderBy('id', 'ASC')->get();
        if ($request->download==1) {
          $totalQuery = count($datas);
          $while = ceil($totalQuery / 500);
          $collections = collect($datas);
          return Excel::create($from.'-'.$until, function ($excel) use ($while, $collections) {
              for ($i = 1; $i <= $while; $i++) {
                  $items = $collections->forPage($i, 500);
                  $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                      $sheet->loadView('administrator.report._report_penjualan_ppob', ['datas' => $items]);
                  });
              }
          })->export('xls');
        }
        $from = date('d-m-Y',strtotime($from));
        $until = date('d-m-Y',strtotime($until));
        return view('administrator.laporan_ppob', compact('dashboard', 'datas','from','until','stts'));
    }
}
