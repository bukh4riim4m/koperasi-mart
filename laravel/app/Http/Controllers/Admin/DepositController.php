<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Deposit;
use App\User;
use App\Menutransaksi;
use App\Bukusaldotransaksi;
use App\Bank;
use App\Website;
use Excel;
use Auth;
use DB;
use Log;

class DepositController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function topup(Request $request)
    {
        $dashboard = 'deposit';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        $nominal= str_replace(".", "", $request->nominal);
        $uniks = substr($nominal, -4);
        $digit = substr($request->user()->no_anggota, -4);
        $total = 50000+$digit;
        // return $uniks.' '.$digit;

        if ($request->action =='topup') {
            if ($nominal < $total) {
                flash()->overlay('Nominal Kurang.', 'GAGAL');
                return redirect()->back();
            }
            if ($uniks !== $digit) {
                flash()->overlay('Kode Unik tidak sesuai.', 'GAGAL');
                return redirect()->back();
            }
            if (Deposit::where('nominal', $nominal)->where('user_id', $request->user()->id)->where('bank', 'LIKE', '%'.$request->bank.'%')->where('status', 1)->first()) {
                flash()->overlay('Deposit sebelumnya masih dalam pengecekan.', 'GAGAL');
                return redirect()->back();
            }
            $banks = Bank::where('bank', $request->bank)->first();
            $create = Deposit::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=>$request->user()->no_anggota,
              'no_trx'=>date('Ymd').rand(0, 100),
              'tgl_trx'=>date('Y-m-d'),
              'bank'=>$banks->bank.' : '.$banks->no_rekening,
              'nominal'=> $nominal,
              'status'=>1,
              'aktif'=>1,
              'created_by'=>$request->user()->id
            ]);
            if ($create) {
                flash()->overlay('Konfirmasi berhasil di kirim.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Konfirmasi Gagal di kirim.', 'INFO');
            return redirect()->back();
        }
        $datas = Deposit::where('aktif', 1)->where('no_anggota', $request->user()->no_anggota)->orderBy('id', 'DESC')->get();
        return view('administrator.datadiri.topup_deposit', compact('dashboard', 'menus', 'datas'));
    }
    public function rekening(Request $request)
    {
        $dashboard = 'dataBank';
        if ($request->action == 'tambah') {
            $berhasi = Bank::create([
              'bank'=>$request->bank,
              'no_rekening'=>$request->rekening,
              'atas_nama'=>$request->atas_nama,
              'created_by'=>$request->user()->id
            ]);
            if ($berhasi) {
                flash()->overlay('Rekening diSimpan.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Rekening Gagal diSimpan.', 'INFO');
            return redirect()->back();
        } elseif ($request->action == 'edit') {
            $edit = Bank::find($request->ids)->update([
              'bank'=>$request->bank,
              'no_rekening'=>$request->rekening,
              'atas_nama'=>$request->atas_nama,
              'updated_by'=>$request->user()->id
            ]);
            if ($edit) {
                flash()->overlay('Rekening Berhasil Edit.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Rekening Gagal Edit.', 'INFO');
            return redirect()->back();
        } elseif ($request->action == 'hapus') {
            $hapus = Bank::find($request->ids)->delete();
            if ($hapus) {
                flash()->overlay('Rekening Berhasil Hapus.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Rekening Gagal Hapus.', 'INFO');
            return redirect()->back();
        }
        $rekenings = Bank::where('aktif', 1)->get();
        return view('administrator.dataBank', compact('dashboard', 'rekenings'));
    }
    public function selectbank(Request $request)
    {
        if ($request->ajax()) {
            Log::info('BANK = '.$request->bank);
            $bankss = Bank::where('bank', $request->bank)->first();
            Log::info('$detail = '.$bankss);
            $option = "";
            $option.="<label>Rekening : ".$bankss->no_rekening."</label>";
            return $option;
        }
    }
    public function konfdep(Request $request)
    {
        $dashboard = 'Konfirmasideposit';
        if ($request->action =='proses') {
            DB::beginTransaction();
            try {
                $deposit = Deposit::where('aktif', 1)->where('status', 1)->where('id', $request->ids)->first();
                $deposit->status = 2;
                $deposit->updated_by = $request->user()->id;
                $saldo = User::find($deposit->user_id);
                $totalsaldo = $saldo->saldotransaksi + $deposit->nominal;
                $saldo->saldotransaksi = $totalsaldo;
                $buku = Bukusaldotransaksi::create([
                  'user_id'=>$saldo->id,
                  'no_anggota'=>$saldo->no_anggota,
                  'tgl_trx'=>date('Y-m-d'),
                  'no_trx'=>date('YmdHis'),
                  'nominal'=>$deposit->nominal,
                  'saldo'=>$totalsaldo,
                  'mutasi'=>'Kredit',
                  'keterangan'=>'Topup Deposit '.$deposit->bank,
                  'aktif'=>1,
                  'created_by'=>$request->user()->id
                ]);
                $deposit->update();
                $saldo->update();
            } catch (\Exception $e) {
                Log::info('Gagal Transaksi Deposit:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal diproses.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Saldo berhasil Upproved.', 'INFO');
            return redirect()->back();
        } elseif ($request->action =='batal') {
            $deposit = Deposit::find($request->ids)->update([
        'status'=>3
      ]);
            if ($deposit) {
                flash()->overlay('Berhasil di batalkan.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Gagal di batalkan.', 'INFO');
            return redirect()->back();
        }
        $deposits = Deposit::where('aktif', 1)->where('status', 1)->orderBy('id', 'DESC')->get();
        return view('administrator.konfirmasi_deposit', compact('dashboard', 'deposits'));
    }
    public function laporan(Request $request)
    {
        $dashboard = 'Konfirmasideposit';
        $dari = date('Y-m-d');
        $sampai = date('Y-m-d');
        $stts = "";
        $deposits = Deposit::whereBetWeen('tgl_trx', [$dari,$sampai])->where('aktif', 1)->where('status', '<>', 1)->orderBy('id', 'DESC')->get();
        if ($request->action == 'cari') {
            $stts = $request->status;
            $dari = date('Y-m-d', strtotime($request->dari));
            $sampai = date('Y-m-d', strtotime($request->sampai));
            $deposits = Deposit::whereBetWeen('tgl_trx', [$dari,$sampai])->where('aktif', 1)->where('status', '<>', 1)->where('status', 'LIKE', '%'.$stts.'%')->orderBy('id', 'DESC')->get();
            if ($request->export =='download') {
                return Excel::create($request->dari.'-'.$request->sampai, function ($excel) use ($stts,$dari,$sampai,$deposits) {
                    $excel->sheet('Excel sheet', function ($sheet) use ($stts,$dari,$sampai,$deposits) {
                        $sheet->loadView('administrator.report._report_deposit_anggota', compact('deposits', 'dari', 'sampai', 'stts', 'stts', 'wajib', 'cari', 'simpanan'));
                    });
                })->export('xls');
            }
        }

        return view('administrator.laporan_deposit', compact('dashboard', 'deposits', 'stts', 'dari', 'sampai'));
    }
    public function topupsuplayer(Request $request)
    {
        $dashboard = 'datasuplayer';
        if ($request->action =='topup') {
            $trxid = date('YmdHis').$request->user()->id;
            $nominal= str_replace(".", "", $request->nominal);
            $params['trxid']=$trxid;
            $params['bank']=$request->bank;
            $params['nominal']=$nominal;
            $access_token = Website::find(1);
            $url = $access_token->url.'topup-suplayer';
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', $url, [
                                    'headers'=>[
                                        'Accept' => 'application/json',
                                        'Content-Type' => 'application/json',
                                        'Authorization' => 'Bearer '.$access_token->token
                                    ],
                                          'body'=>json_encode($params)
                                ]);
            if ($res->getStatusCode() == 200) {
                $hasil =  json_decode($res->getBody()->getContents(), true);
                if ($hasil['result'] =='success') {
                    flash()->overlay($hasil['message'], 'INFO');
                    return redirect()->back();
                } else {
                    // Silakan transfer sebesar Rp 500.507,- Ke Rekening: BCA, no. 0770441111, a.n. BENY ARIF L. Batas waktu transfer 1x24jam
                    flash()->overlay($hasil['message'], 'GAGAL');
                    return redirect()->back();
                }
            } else {
                flash()->overlay('Gagal menampilkan data', 'INFO');
                return redirect()->back();
            }
        }
        $from = date('Y-m-01');
        $until = date('Y-m-d');
        $dari = date('01-m-Y', strtotime($from));
        $sampai = date('d-m-Y', strtotime($until));
        if ($request->action == 'cari') {
            $from = date('Y-m-d', strtotime($request->from));
            $until = date('Y-m-d', strtotime($request->until));
            $dari = date('d-m-Y', strtotime($from));
            $sampai = date('d-m-Y', strtotime($until));
        }

        $status = $request->status;
        $params['status']=$status;
        $params['from']=$from;
        $params['until']=$until;
        $access_token = Website::find(1);
        $url = $access_token->url.'data-suplayer';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
                                'headers'=>[
                                    'Accept' => 'application/json',
                                    'Content-Type' => 'application/json',
                                    'Authorization' => 'Bearer '.$access_token->token
                                ],
                                      'body'=>json_encode($params)
                            ]);
        if ($res->getStatusCode() == 200) {
            $hasil =  json_decode($res->getBody()->getContents(), true);
        } else {
            flash()->overlay('Gagal menampilkan ddata', 'INFO');
            return redirect()->back();
        }
        return view('administrator.suplayers.topup_deposit', compact('dashboard', 'hasil', 'dari', 'sampai', 'status'));
    }
    public function bukusaldosuplayer(Request $request)
    {
        // return 'OK';
        $from = date('Y-m-01');
        $until = date('Y-m-d');
        if ($request->dari) {
            $from = date('Y-m-d', strtotime($request->dari));
            $until = date('Y-m-d', strtotime($request->sampai));
        }
        $dari = date('d-m-Y', strtotime($from));
        $sampai = date('d-m-Y', strtotime($until));
        // ??ppppppppppppppppppppppppppppp
        $params['from']=$from;
        $params['until']=$until;
        $access_token = Website::find(1);
        $url = $access_token->url.'buku-saldo-suplayer';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
                                'headers'=>[
                                    'Accept' => 'application/json',
                                    'Content-Type' => 'application/json',
                                    'Authorization' => 'Bearer '.$access_token->token
                                ],
                                      'body'=>json_encode($params)
                            ]);
        if ($res->getStatusCode() == 200) {
            $hasil =  json_decode($res->getBody()->getContents(), true);
            $hasiluser = $hasil['users'];
            $hasilbukusaldo = $hasil['bukusaldo'];
            $dashboard = "datasuplayer";
        } else {
            flash()->overlay('Gagal menampilkan ddata', 'INFO');
            return redirect()->back();
        }
        return view('administrator.suplayers.buku_saldo', compact('dashboard', 'hasiluser', 'hasilbukusaldo', 'dari', 'sampai'));
    }
    public function bukusaldoanggota(Request $request)
    {
        $dashboard = 'bukuSaldo';
        if (!$users = User::where('no_anggota', 'LIKE', '%'.$request->no_anggota.'%')->first()) {
            flash()->overlay('Nomor Anggot Salah.', 'INFO');
            return redirect()->back();
        };
        $no_anggota = $users->no_anggota;
        $from = date('01-01-Y');
        $until = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($until));
        if ($request->tipe == 'mutasi') {
            $this->validate($request, [
              'mutasi'=>'required',
              'no_anggota'=>'required',
              'nominal'=>'required',
              'keterangan'=>'required'
            ]);
            if ($request->mutasi == 'Kredit') {
                DB::beginTransaction();
                try {
                    $user = User::where('no_anggota', $request->no_anggota)->where('aktif', 1)->first();
                    $user->saldotransaksi = $user->saldotransaksi + $request->nominal;
                    $saldo = Bukusaldotransaksi::create([
                      'user_id'=>$user->id,
                      'no_anggota'=>$user->no_anggota,
                      'tgl_trx'=>date('Y-m-d'),
                      'no_trx'=>date('ymdHis').$user->id,
                      'nominal'=>$request->nominal,
                      'saldo'=>(int)$user->saldotransaksi + $request->nominal,
                      'mutasi'=>'Kredit',
                      'keterangan'=>$request->keterangan,
                      'aktif'=>1
                    ]);
                    $user->update();
                } catch (\Exception $e) {
                    Log::info('Gagal MANUAL:'.$e->getMessage());
                    DB::rollback();
                    flash()->overlay('Mutasi GAGAL.', 'INFO');
                    return redirect()->back();
                }
                DB::commit();
                flash()->overlay('Mutasi Berhasil.', 'INFO');
                return redirect()->back();
            } else {
                DB::beginTransaction();
                try {
                    $user = User::where('no_anggota', $request->no_anggota)->where('aktif', 1)->first();
                    $user->saldotransaksi = $user->saldotransaksi - $request->nominal;
                    $saldo = Bukusaldotransaksi::create([
                    'user_id'=>$user->id,
                    'no_anggota'=>$user->no_anggota,
                    'tgl_trx'=>date('Y-m-d'),
                    'no_trx'=>date('ymdHis').$user->id,
                    'nominal'=>$request->nominal,
                    'saldo'=>(int)$user->saldotransaksi - $request->nominal,
                    'mutasi'=>'Kredit',
                    'keterangan'=>$request->keterangan,
                    'aktif'=>1
                  ]);
                    $user->update();
                } catch (\Exception $e) {
                    Log::info('Gagal MANUAL:'.$e->getMessage());
                    DB::rollback();
                    flash()->overlay('Mutasi GAGAL.', 'INFO');
                    return redirect()->back();
                }
                DB::commit();
                flash()->overlay('Mutasi Berhasil.', 'INFO');
                return redirect()->back();
            }
        } elseif ($request->tipe == 'topup') {
            DB::beginTransaction();
            try {
                $user = User::where('no_anggota', $request->no_anggota)->where('aktif', 1)->first();
                $user->saldotransaksi = $user->saldotransaksi + $request->nominal;
                $saldo = Bukusaldotransaksi::create([
                  'user_id'=>$user->id,
                  'no_anggota'=>$user->no_anggota,
                  'tgl_trx'=>date('Y-m-d'),
                  'no_trx'=>date('ymdHis').$user->id,
                  'nominal'=>$request->nominal,
                  'saldo'=>(int)$user->saldotransaksi + $request->nominal,
                  'mutasi'=>'Kredit',
                  'keterangan'=>'Topup Deposi Anggota '.$user->no_anggota,
                  'aktif'=>1
                ]);
                $user->update();
                $deposit = Deposit::create([
                  'user_id'=>$user->id,
                  'no_anggota'=>$user->no_anggota,
                  'tgl_trx'=>date('Y-m-d'),
                  'no_trx'=>date('ymdHis').$user->id,
                  'nominal'=>$request->nominal,
                  'bank'=>$request->bank,
                  'status'=>2,
                  'aktif'=>1
                ]);
            } catch (\Exception $e) {
                Log::info('Topup MANUAL:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Mutasi GAGAL.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Mutasi Berhasil.', 'INFO');
            return redirect()->back();
        }

        $datasppob = Bukusaldotransaksi::whereBetWeen('tgl_trx', [$dari,$ke])->where('no_anggota', $no_anggota)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        // $users = User::find($request->user()->id);
        return view('administrator.bukusaldoTransaksi', compact('dashboard', 'from', 'until', 'users', 'datasppob', 'no_anggota'));
    }
}
