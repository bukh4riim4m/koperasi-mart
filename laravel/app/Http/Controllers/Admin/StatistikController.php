<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Akumulasi;
// use Charts;
use App\Charts\SampleChart;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use App\Simpananadmin;
use Excel;
use DB;
use Log;
use PDF;

class StatistikController extends Controller
{
  public function __construct()
  {
      $this->middleware('admin');
  }

  public function statistikakumulasi(Request $request){
    $dashboard ="statistik";
    $dari = date("Y-m-d");
    $sampai = date("Y-m-d");
    if ($request->dari && $request->sampai) {
      $dari = date('Y-m-d', strtotime($request->dari));
      $sampai = date('Y-m-d', strtotime($request->sampai));
    }
    $totalramanuju = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Anggota'");
    $totalgrogol = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Anggota'");
    $totalpci = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Anggota'");
    $totalramanujunon = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Non'");
    $totalgrogolnon = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Non'");
    $totalpcinon = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Non'");
    if ($request->action =='excel' && $request->excel ==1) {
      return Excel::create($dari.'-'.$sampai, function ($excel) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
            $excel->sheet('Excel sheet', function($sheet) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
            $sheet->cells('A1:C1', function($cells) {
              $cells->setBackground('#ffff00');
            });
            $sheet->cells('A6:C6', function($cells) {
              $cells->setBackground('#ffff00');
            });
            $sheet->cells('A8:C8', function($cells) {
              $cells->setBackground('#66ff66');
            });
            $sheet->cells('A13:C13', function($cells) {
              $cells->setBackground('#66ff66');
            });
            $sheet->cells('A15:C15', function($cells) {
              $cells->setBackground('#66ffcc');
            });
            $sheet->cells('A20:C20', function($cells) {
              $cells->setBackground('#66ffcc');
            });
            $sheet->loadView('administrator.report._report_statistik_penjualan', compact('totalramanuju','totalgrogol','totalpci','totalramanujunon','totalgrogolnon','totalpcinon','dari','sampai'));
          });
      })->export('xls');
    }elseif ($request->action =='pdf' && $request->excel ==1) {
      $pdf = PDF::loadView('administrator.report.pdf_statistik_penjualan',compact('totalramanuju','totalgrogol','totalpci','totalramanujunon','totalgrogolnon','totalpcinon','dari','sampai'));
      return $pdf->download($dari.'_'.$sampai.'_Statistik.pdf');
    }
    return view('administrator.statistik',compact('dashboard','totalramanuju','totalgrogol','totalpci','totalramanujunon','totalgrogolnon','totalpcinon','dari','sampai'));
  }

  public function karyawan(Request $request){
    $dashboard ="dataPengguna";
    $nomor =$request->no_karyawan;
    $name =$request->name;
    $type="";
    if ($request->action =='tambah') {
      $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'no_karyawan' => 'required|min:6',
      'id_login' => 'required|min:6',
      'password_login' => 'required|min:6'
      ]);
      if ($idlog = User::where('sequence',$request->id_login)->first()) {
        flash()->overlay('GAGAL, ID Login Sudah di gunakan.','INFO');
        return redirect()->back();
      }
      if ($idemail = User::where('email',$request->email)->first()) {
        flash()->overlay('GAGAL, Email Sudah di gunakan.','INFO');
        return redirect()->back();
      }
      $user = User::create([
        'name' => $request->name,
        'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
        'no_anggota' => $request->no_karyawan,
        'sequence' => $request->id_login,
        'email' => $request->email,
        'telp'=>$request->telp,
        'jenkel' => $request->jenkel,
        'password' => Hash::make($request->password_login),
        'saldo' => 0,
        'admin' => $request->user()->id,
        'aktif' => 1,
        'type' => 'karyawan'
      ]);
      if ($user) {
        flash()->overlay('Karyawan berhasil di tambahkan.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Karyawan gagal di tambahkan.','INFO');
      return redirect()->back();
    }elseif ($request->action =='edit') {
      if ($request->fotodiri) {
        $this->validate($request, [
              'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
      }
      DB::beginTransaction();
      try {
        $user = User::find($request->ids);
        $user->name=$request->name;
        $user->no_anggota=$request->no_karyawan;
        $user->tgl_lahir=date('Y-m-d', strtotime($request->tgl_lahir));
        $user->email=$request->email;
        $user->telp=$request->telp;
        $user->jenkel=$request->jenkel;

        if ($request->fotodiri) {
          if ($user->fotodiri =='null' || $user->fotodiri =='') {
            $user->fotodiri=$diri;
            $destination_foto =public_path('foto');
            $request->fotodiri->move($destination_foto, $diri);
          }else {
            $destination_foto =public_path('foto/'.$user->fotodiri);
            if(file_exists($destination_foto)){
                        unlink($destination_foto);
            }
            $user->fotodiri=$diri;
            $destination_foto =public_path('foto');
            $request->fotodiri->move($destination_foto, $diri);
          }
        }
        $user->update();
      } catch (\Exception $e) {
        Log::info('Gagal Edit Profil:'.$e->getMessage());
        DB::rollback();
        flash()->overlay('Gagal Edit Profil.','INFO');
        return redirect()->back();
      }
      DB::commit();
      flash()->overlay('Data Profil berhasil di Edit.','INFO');
      return redirect()->back();
    }elseif ($request->action =='hapus') {
      $delete = User::find($request->ids);
      $delete->aktif = 0;
      if ($delete->update()) {
        flash()->overlay('Data Profil berhasil di Hapus.','INFO');
        return redirect()->back();
      }
      flash()->overlay('Data Profil Gagal di Hapus.','INFO');
      return redirect()->back();
    }
    $user = User::where('no_anggota','LIKE','%'.$nomor.'%')->where('name','LIKE','%'.$name.'%')->where('aktif',1)->where('type','karyawan')->orderBy('no_anggota','ASC')->get();
    return view('administrator.dataKaryawan',compact('dashboard','user','nomor','name','type'));
  }
  public function chart(Request $request){
    $dashboard = "statistik";
    $lakis = DB::select("SELECT SUM(users.aktif) as totalLaki FROM users WHERE users.aktif ='1' AND users.jenkel = 'Laki-Laki'");
    $perempuan = DB::select("SELECT SUM(users.aktif) as totalPerempuan FROM users WHERE users.aktif ='1' AND users.jenkel = 'Perempuan'");
    return view('administrator.statistikJenkel', compact('dashboard','lakis','perempuan'));
  }
  public function statistikkelurahan(){
    $dashboard = "statistik";
    $kelurahans = Kelurahan::where('aktif',1)->get();
    return view('administrator.statistikKelurahan', compact('dashboard','kelurahans'));

  }
  public function statistikkecamatan(){
    $dashboard = "statistik";
    $kecamatans = Kecamatan::where('aktif',1)->get();
    return view('administrator.statistik_kecamatan', compact('dashboard','kecamatans'));

  }
  public function statistikkabupaten(){
    $dashboard = "statistik";
    $kabupatens = Kabupaten::where('aktif',1)->get();
    return view('administrator.statistk_kabupaten', compact('dashboard','kabupatens'));
  }
  public function statistiksimpanan(Request $request){
    $dashboard = "statistik";
    $pokoks = DB::select("SELECT SUM(simpananadmins.nominal) as totalpokok FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '1' AND simpananadmins.mutasi = 'Kredit'");
    $wajins = DB::select("SELECT SUM(simpananadmins.nominal) as totalwajib FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '2' AND simpananadmins.mutasi = 'Kredit'");
    $sukarelas = DB::select("SELECT SUM(simpananadmins.nominal) as totalsukarela FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '3' AND simpananadmins.mutasi = 'Kredit'");
    $investasis = DB::select("SELECT SUM(simpananadmins.nominal) as totalinvestasi FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '4' AND simpananadmins.mutasi = 'Kredit'");
    $wakafs = DB::select("SELECT SUM(simpananadmins.nominal) as totalwakaf FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '5' AND simpananadmins.mutasi = 'Kredit'");
    $infaqs = DB::select("SELECT SUM(simpananadmins.nominal) as totalinfak FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '6' AND simpananadmins.mutasi = 'Kredit'");
    $shus = DB::select("SELECT SUM(simpananadmins.nominal) as totalshu FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '7' AND simpananadmins.mutasi = 'Kredit'");
    $lains = DB::select("SELECT SUM(simpananadmins.nominal) as totallain FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '8' AND simpananadmins.mutasi = 'Kredit'");
    return view('administrator.statistik_simpanan', compact('dashboard','pokoks','wajins','sukarelas','investasis','wakafs','infaqs','shus','lains'));
  }
}
