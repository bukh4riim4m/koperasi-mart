<?php

namespace App\Http\Controllers\Pengurus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pendidikan;
use App\Komunitas;
use App\Kelurahan;
use App\Kecamatan;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\Kabupaten;
use App\Bank;

class DataMasterController extends Controller
{
  public function __construct()
  {
      $this->middleware('pengurus');
  }
  public function pendidikan(Request $request){
    $dashboard ="dataMaster";
    $pendidikans = Pendidikan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPendidikan',compact('dashboard','pendidikans'));
  }
  public function komunitas(Request $request){
    $dashboard ="dataMaster";
    $komunitass = Komunitas::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKomunitas',compact('dashboard','komunitass'));
  }
  public function kelurahan(Request $request){
    $dashboard ="dataMaster";
    $kelurahans = Kelurahan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKelurahan',compact('dashboard','kelurahans'));
  }
  public function kecamatan(Request $request){
    $dashboard ="dataMaster";
    $kecamatans = Kecamatan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKecamatan',compact('dashboard','kecamatans'));
  }
  public function kabupaten(Request $request){
    $dashboard ="dataMaster";
    $kabupatens = Kabupaten::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataKabupaten',compact('dashboard','kabupatens'));
  }
  public function propinsi(Request $request){
    $dashboard ="dataMaster";
    $propinsis = Propinsi::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPropinsi',compact('dashboard','propinsis'));
  }
  public function pendapatan(Request $request){
    $dashboard ="dataMaster";
    $pendapatans = Pendapatan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPendapatan',compact('dashboard','pendapatans'));
  }
  public function pekerjaan(Request $request){
    $dashboard ="dataMaster";
    $pekerjaans = pekerjaan::where('aktif',1)->get();
    return view('pengurus.dataMaster.dataPekerjaan',compact('dashboard','pekerjaans'));
  }
  public function jenissimpanan(Request $request){
    $dashboard ="dataMaster";
    $jenissimpanans = jenissimpanan::where('aktif',1)->get();
    return view('pengurus.dataMaster.jenisSimpanan',compact('dashboard','jenissimpanans'));
  }
  public function rekening(Request $request)
  {
      $dashboard = 'dataBank';
      $rekenings = Bank::where('aktif', 1)->get();
      return view('pengurus.dataMaster.dataBank', compact('dashboard', 'rekenings'));
  }
}
