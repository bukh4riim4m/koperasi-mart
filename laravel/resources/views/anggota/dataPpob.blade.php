@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-12">
		<h4 class="page-title">Data Transaksi PPOB</h4>
	</div>
  <!-- <div class="col-xs-6">
    <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#download"><i class="fa fa-download"></i> Download</a>
  </div> -->
</div>

<div class="row filter-row">
  <form class="m-b-30" action="{{route('anggota-data-transaksi-ppob')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="until" value="{{$until}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-12">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status</label>
        <?php $statuses = ['Proses','Berhasil','Gagal']; ?>
        <select class="select floating" name="status">
          <option value="">-- Semua --</option>
          @foreach($statuses as $key=>$status)
          @if($status == $stts)
          <option value="{{$status}}" selected>{{$status}}</option>
          @else
          <option value="{{$status}}">{{$status}}</option>
          @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
    </div>
  </form>
</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped custom-table datatable">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
						<th>No Transaksi</th>
						<th>Tgl Transaksi</th>
            <th>Nomor Pelanggan</th>
            <th>Paket</th>
            <th>Harga</th>
            <th>Status</th>
            <th style="max-width:70px;">Action</th>
					</tr>
				</thead>
				<tbody>
          <?php $no=1; ?>
          @foreach($datas as $data)
					<tr class="holiday-completed">
						<td>{{$no++}}</td>
						<td>{{$data->no_trx}}</td>
            <td>{{date('d-m-Y', strtotime($data->tgl_trx))}}</td>
            <td>{{$data->nomorhp}}</td>
            <td>{{$data->paket}}</td>
            <td>Rp {{number_format($data->nominal,0,",",".")}}</td>
            <td>@if($data->status =='Proses')<span class="label label-warning-border">{{$data->status}}</span>@elseif($data->status =='Berhasil')<span class="label label-success-border">{{$data->status}}</span>@else <span class="label label-danger-border">{{$data->status}}</span>@endif</td>
						<td style="max-width:70px;">
              <form class="" action="{{route('anggota-cek-status')}}" method="post" id="cek-status{{$data->id}}">
                @csrf
                <input type="hidden" name="ids" value="{{$data->id}}">
              </form>
              @if($data->status =='Proses')
							<!-- <a href="javascript:void(0);" onclick="event.preventDefault(); document.getElementById('cek-status{{$data->id}}').submit();" class="btn btn-primary btn-sm rounded" >Refresh</a> -->
              @elseif($data->status =='Berhasil')
              <a href="javascript:void(0);" data-toggle="modal" data-target="#sn{{$data->id}}" class="btn btn-primary btn-sm rounded" >SN</a>
              @endif
            </td>
					</tr>
          @endforeach
          @if(count($datas) < 1)
          <tr>
            <td colspan="8" align="center">Data Kosong</td>
          </tr>
          @endif
				</tbody>

			</table>
		</div>
	</div>
</div>
@foreach($datas as $sn)
<div id="sn{{$sn->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title"> Serial Number</h4>
      </div>
        <div class="modal-body card-box">
          <table width="100%" class="table table-striped">
            <tr>
              <td>
                <p>
                  Berikut Serial number : <br><br>
                  <h4>{{$sn->sn}}</h4>
                </p>
              </td>
            </tr>
          </table>
          <div class="m-t-20"> <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
          </div>
        </div>
    </div>
  </div>
</div>
@endforeach
</div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
