@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-6">
          <h4 class="page-title">DATA TOPUP</h4>
        </div>
        <div class="col-xs-6">
          <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#download"><i class="fa fa-download"></i> Download</a>
        </div>
      </div>
      <div class="row filter-row">
        <form class="form" action="{{route('data.topup')}}" method="post">
          <input type="hidden" name="action" value="cari">
          @csrf

          <div class="col-sm-4 col-md-3 col-xs-6">
            <div class="form-group form-focus">
              <label class="control-label">Dari Tgl</label>
              <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$from}}"></div>
            </div>
          </div>
          <div class="col-sm-4 col-md-3 col-xs-6">
            <div class="form-group form-focus">
              <label class="control-label">Sampai Tgl</label>
              <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="to" value="{{$to}}"></div>
            </div>
          </div>
          <div class="col-sm-4 col-md-3 col-xs-6">
            <div class="form-group form-focus select-focus">
              <label class="control-label">Status</label>
              <?php $jenis = App\Statusdeposit::where('aktif', 1)->get(); ?>
              <select class="select floating" name="status">
                <option value=""> -- Semua -- </option>
                @foreach($jenis as $jen)
                  @if($status == $jen->id)
                    <option value="{{$jen->id}}" selected> {{$jen->status}} </option>
                  @else
                  <option value="{{$jen->id}}"> {{$jen->status}} </option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-3 col-xs-6">
            <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
          </div>
        </form>
      </div>
    <div class="row">
      <div class="col-sm-8 col-md-12 col-xs-12">
        <table class="table table-striped custom-table">
          <thead>
          <tr>
            <th>No.</th>
            <th>Tgl Transaksi</th>
            <th>No. Transaksi</th>
            <th>Bank</th>
            <th>Nominal</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; $total =0;?>
          @foreach($datas as $data)
          <?php $total+= $data->nominal;?>
          <tr>
            <td>{{$no++}}.</td>
            <td>{{date('d-m-Y', strtotime($data->tgl_trx))}}</td>
            <td>{{$data->no_trx}}</td>
            <td>{{$data->bank}}</td>
            <td>Rp {{number_format($data->nominal,0,",",".")}}</td>
            <td>@if($data->status ==1)<span class="label label-warning-border">{{$data->statusId->status}}</span>@elseif($data->status ==2)<span class="label label-success-border">{{$data->statusId->status}}</span>@else <span class="label label-danger-border">{{$data->statusId->status}}</span>@endif</td>
          </tr>
          @endforeach

          @if(count($datas) < 1)
          <tr>
            <td colspan="6" align="center">DATA KOSONG</td>
          </tr>
          @else
          <thead>
          <tr>
            <th colspan="4">Total</th>
            <th>Rp {{number_format($total,0,",",".")}}</th>
            <th></th>
          </tr>
          </thead>
          @endif
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
