@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="page-title">Statistik Penjualan</h4>
        </div>
      </div>
      <div class="row filter-row">
        <form class="form" action="{{('/anggota/statistik-penjualan')}}" method="post">
          @csrf
          <div class="col-sm-4 col-xs-6">
      			<div class="form-group form-focus">
      				<label class="control-label">Dari</label>
      				<div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{date('d-m-Y', strtotime($dari))}}"></div>
      			</div>
      		</div>
          <div class="col-sm-4 col-xs-6">
      			<div class="form-group form-focus">
      				<label class="control-label">Sampai</label>
      				<div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{date('d-m-Y', strtotime($sampai))}}"></div>
      			</div>
      		</div>
          <div class="col-sm-4 col-xs-12">
            <input type="submit" class="btn btn-success btn-block" name="btn" value="Tampilkan"/>
          </div>
        </form>
        <div class="col-sm-4 col-xs-12"><br>
          <form class="" action="{{url('/anggota/statistik-penjualan')}}" method="post" id="export">
            @csrf
            <input type="hidden" name="dari" value="{{date('d-m-Y', strtotime($dari))}}"/>
            <input type="hidden" name="sampai" value="{{date('d-m-Y', strtotime($sampai))}}"/>
            <input type="hidden" name="excel" value="1"/>
            <input type="hidden" name="action" value="excel"/>
          </form>
          <form class="" action="{{url('/anggota/statistik-penjualan')}}" method="post" id="pdf">
            @csrf
            <input type="hidden" name="dari" value="{{date('d-m-Y', strtotime($dari))}}"/>
            <input type="hidden" name="sampai" value="{{date('d-m-Y', strtotime($sampai))}}"/>
            <input type="hidden" name="excel" value="1"/>
            <input type="hidden" name="action" value="pdf"/>
          </form>
        <a href="#" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                      document.getElementById('export').submit();"/></a>
                      <a href="#" class="pull-right"><img width="40px" src="{{url('/images/hasil.png')}}" onclick="event.preventDefault();
                                    document.getElementById('pdf').submit();"/></a>
                    </div>
      </div>
      <div class="row"><br>
        <div class="col-md-4">
          <div class="panel panel-table">
            <div class="panel-heading">
              <h3 class="panel-title">Penjualan ke Anggota :</h3> <br><p>{{date('d-M-Y', strtotime($dari))}}  s/d  {{date('d-M-Y', strtotime($sampai))}}</p>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped custom-table m-b-0">
                  <thead>
                    <tr>
                      <th><strong >No.</strong></th>
                      <th><strong>Gerai</strong></th>
                      <th><strong>Nominal</strong></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td>
                        <h2>Ramanuju</h2>
                      </td>
                      <td>Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>
                        <h2>Grogol</h2>
                      </td>
                      <td>Rp {{number_format($totalgrogol[0]->totalGrogolAnggota,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>
                        <h2>PCI</h2>
                      </td>
                      <td>Rp {{number_format($totalpci[0]->totalPciAnggota,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <th colspan="2">Total</th>
                      <th>Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota+$totalgrogol[0]->totalGrogolAnggota+$totalpci[0]->totalPciAnggota,0,",",".")}}</th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-table">
            <div class="panel-heading">
              <h3 class="panel-title">Penjualan ke Non Anggota :</h3> <br><p>{{date('d-M-Y', strtotime($dari))}}  s/d  {{date('d-M-Y', strtotime($sampai))}}</p>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped custom-table m-b-0">
                  <thead>
                    <tr>
                      <th><strong>No.</strong></th>
                      <th><strong>Gerai</strong></th>
                      <th><strong>Nominal</strong></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td>
                        <h2>Ramanuju</h2>
                      </td>
                      <td>Rp {{number_format($totalramanujunon[0]->totalRamanujuAnggota,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>
                        <h2>Grorol</h2>
                      </td>
                      <td>Rp {{number_format($totalgrogolnon[0]->totalGrogolAnggota,0,",",".")}}</td>
                    </tr><tr>
                      <td>3.</td>
                      <td>
                        <h2>PCI</h2>
                      </td>
                      <td>Rp {{number_format($totalpcinon[0]->totalPciAnggota,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <th colspan="2">Total</th>
                      <th>Rp {{number_format($totalramanujunon[0]->totalRamanujuAnggota+$totalgrogolnon[0]->totalGrogolAnggota+$totalpcinon[0]->totalPciAnggota,0,",",".")}}</th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-table">
            <div class="panel-heading">
              <h3 class="panel-title">Total Penjualan :</h3> <br><p>{{date('d-M-Y', strtotime($dari))}}  s/d  {{date('d-M-Y', strtotime($sampai))}}</p>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped custom-table m-b-0">
                  <thead>
                    <tr>
                      <th><strong>No.</strong></th>
                      <th><strong>Gerai</strong></th>
                      <th><strong>Nominal</strong></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td>
                        <h2>Ramanuju</h2>
                      </td>
                      <td>Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota+$totalramanujunon[0]->totalRamanujuAnggota,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>
                        <h2>Grorol</h2>
                      </td>
                      <td>Rp {{number_format($totalgrogol[0]->totalGrogolAnggota+$totalgrogolnon[0]->totalGrogolAnggota,0,",",".")}}</td>
                    </tr><tr>
                      <td>3.</td>
                      <td>
                        <h2>PCI</h2>
                      </td>
                      <td>Rp {{number_format($totalpci[0]->totalPciAnggota+$totalpcinon[0]->totalPciAnggota,0,",",".")}}</td>
                    </tr>
                    <tr>
                      <th colspan="2">Total</th>
                      <th>Rp {{number_format($totalramanuju[0]->totalRamanujuAnggota+$totalgrogol[0]->totalGrogolAnggota+$totalpci[0]->totalPciAnggota+$totalramanujunon[0]->totalRamanujuAnggota+$totalgrogolnon[0]->totalGrogolAnggota+$totalpcinon[0]->totalPciAnggota,0,",",".")}}</th>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
</div>
            <!-- CREATE INFORMASI -->

        </div>

		<div class="sidebar-overlay" data-reff="#sidebar"></div>

@endsection
