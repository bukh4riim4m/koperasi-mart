@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Akumulasi Belanja</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_akumulasi"><i class="fa fa-plus"></i> Tambah Akumulasi</a>
  <div class="view-icons">
  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No.KSSD</label>
        <input type="text" class="form-control floating" name="no_kssd" value="{{$no_kssd}}" minlength="6"/>
      </div>
    </div>
    <div class="col-sm-4 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="to" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Type</label>
        <select class="select floating" name="type">
          <option value=""> Semua Type </option>
          <option value="Anggota"> Anggota</option>
          <option value="Non"> Non Anggota </option>
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-3 col-xs-12"><br>
  <a href="{{url('/administrator/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export').submit();"/></a>
              </div>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{url('/administrator/akumulasi-belanja')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="action" value="export">
    <input type="hidden" name="no_kssd" value="{{$no_kssd}}"/>
    <input type="hidden" name="from" value="{{$from}}"/>
    <input type="hidden" name="to" value="{{$to}}"/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.KSSD</th>
          <th>No.Transaksi</th>
          <th>Tgl. Transaksi</th>
          <th>Nominal</th>
          <th>Gerai</th>
          <th>Type</th>
          <th style="max-width:110px;" class="text-center">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $total=0;?>
        @foreach($akumulasis as $akumulasi)
        <?php $total+= $akumulasi->nominal;?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$akumulasi->no_kssd}}</td>
          <td>{{$akumulasi->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($akumulasi->tgl_trx))}}</td>
          <td>Rp {{number_format($akumulasi->nominal,0,",",".")}}</td>
          <td>{{$akumulasi->gerai}}</td>
          <td>@if($akumulasi->type =='Non')Non Anggota @else {{$akumulasi->type}} @endif</td>
          <td style="max-width:110px;" align="center">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_komunitas{{$akumulasi->id}}">Edit</a>
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_komunitas{{$akumulasi->id}}">Hapus</a>
          </td>
        </tr>
        @endforeach
        @if(count($akumulasis) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Akumulasi</td>
    <td>: Rp {{number_format($total,0,",",".")}}</td>
  </tr>
</table>

</div>

</div>
<!-- <label class="col-lg-3 control-label">Light Logo</label> -->
<form class="" action="{{url('administrator/akumulasi-belanja')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-9"><br>
  </div>
  <div class="col-lg-2"><br>
      <input class="form-control rounded btn-success" type="submit" name="csv" value="Upload">
  </div>
</form>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>

  <div id="add_akumulasi" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-content modal-md">
        <div class="modal-header">
          <h4 class="modal-title">Form Akumulasi Belanja</h4>
        </div>
        <div class="modal-body">
          <form action="{{url('/administrator/akumulasi-belanja')}}" method="post">
            <input type="hidden" name="action" value="tambah">
              @csrf
              <div class="col-md-12 col-xs-12">
                <div class="form-group form-focus">
                  <label class="control-label">Tanggal Transaksi</label>
                  <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="tgl_trx" value="{{date('d-m-Y')}}" required></div>
                </div>
              </div>
              <div class="col-md-6 col-xs-6">
                <div class="form-group">
                  <label>No.KSSD <span class="text-danger">*</span></label>
                  <input class="input-sm form-control" required="" type="text" name="no_kssd"  minlength="6" required>
                </div>
              </div>
              <div class="col-md-6 col-xs-6">
                <div class="form-group">
                  <label>Gerai <span class="text-danger">*</span></label>
                  <input class="input-sm form-control" required="" type="text" name="gerai" required>
                </div>
              </div>
              <div class="col-md-6 col-xs-6">
                <div class="form-group">
                  <label>Type <span class="text-danger">*</span></label>
                  <select class="form-control" name="type" required>
                    <option value="Anggota">Anggota</option>
                    <option value="Non Anggota">Non Anggota</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6 col-xs-6">
                <div class="form-group">
                  <label>Nominal <span class="text-danger">*</span></label>
                  <input class="input-sm form-control" required="" type="number" name="nominal" required>
                </div>
              </div>
            <div class="m-t-20 text-center">
              <input class="btn btn-primary" type="submit" value="SIMPAN"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  @foreach($akumulasis as $dta)
  <div id="edit_komunitas{{$dta->id}}" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div class="modal-content modal-md">
        <div class="modal-header">
          <h4 class="modal-title">Edit Akumulasi Belanja</h4>
        </div>
        <div class="modal-body">
          <form action="{{url('/administrator/akumulasi-belanja')}}" method="post">
            @csrf
            <div class="form-group">
              <div class="form-group form-focus">
                <label class="control-label">Tanggal Transaksi</label>
                <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="tgl_trx" value="{{date('d-m-Y', strtotime($dta->tgl_trx))}}" required></div>
              </div>
              <input type="hidden" name="ids" value="{{$dta->id}}">
              <input type="hidden" name="action" value="edit">
              <!-- <input type="hidden" name="tahun" value=""> -->
            </div>
            <div class="form-group">
              <label>No.Anggota <span class="text-danger">*</span></label>
              <input class="input-sm form-control" name="no_kssd" value="{{$dta->no_kssd}}" type="text" required>
            </div>
            <div class="form-group">
              <label>Nominal <span class="text-danger">*</span></label>
              <input class="input-sm form-control" name="nominal" value="{{$dta->nominal}}" type="text" required>
            </div>
            <div class="form-group">
              <label>Gerai <span class="text-danger">*</span></label>
              <input class="input-sm form-control" name="gerai" value="{{$dta->gerai}}" type="text" required>
            </div>
            <div class="m-t-20 text-center">
              <button class="btn btn-success">SIMPAN</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="hapus_komunitas{{$dta->id}}" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content modal-md">
        <div class="modal-header">
          <h4 class="modal-title">Hapus Saldo Akumulasi</h4>
        </div>
        <form action="{{url('/administrator/akumulasi-belanja')}}" method="post" id="hapus_golongan">
          <input type="hidden" name="ids" value="{{$dta->id}}">
          <input type="hidden" name="action" value="hapus">
          <!-- <input type="hidden" name="tahun" value="">
          <input type="hidden" name="no_anggota" value=""> -->
          @csrf
          <div class="modal-body card-box">
            <p>Apakah yakin ingin di Hapus : </p>
            <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Kembali</a>
              <button type="submit" class="btn btn-danger">Hapus</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  @endforeach
@endsection
