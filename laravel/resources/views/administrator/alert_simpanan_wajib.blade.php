@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Simpanan Wajib Belum Bayar</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Simpanan</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{url('/administrator/status-simpanan-wajib')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-3 col-md-3 col-xs-12">
  		<div class="form-group form-focus">
  			<label class="control-label">No Anggota</label>
  			<input type="text" name="no_anggota" value="{{$no_anggota}}" class="form-control floating">
  		</div>
  	</div>

    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Tahun</label>
        <?php $tahuns = App\Tahun::get(); ?>
        <select class="select floating" name="tahun">
          @foreach($tahuns as $tahun)
            @if($alertbulan == $tahun->name)
              <option value="{{$tahun->name}}" selected> {{$tahun->name}} </option>
            @else
              <option value="{{$tahun->name}}"> {{$tahun->name}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status</label>
        <?php $statuses = ['Semua','Sudah Bayar','Belum Bayar']; ?>
        <select class="select floating" name="status">
          @foreach($statuses as $status)
            @if($stts == $status)
              <option value="{{$status}}" selected> {{$status}} </option>
            @else
              <option value="{{$status}}"> {{$status}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-3 col-xs-12"><br>
    <form class="" action="{{url('/administrator/status-simpanan-wajib')}}" method="post" id="export">
      @csrf
      <input type="hidden" name="action" value="export">
      <input type="hidden" name="tahun" value="{{$alertbulan}}">
      <input type="hidden" name="no_anggota" value="{{$no_anggota}}">
      <input type="hidden" name="status" value="{{$stts}}">
      <input type="hidden" name="bulan" value="{{$bln}}">
      <input type="hidden" name="excel" value="1">
    </form>
  <a href="{{url('administrator/simpanan-wajib-belum-bayar')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export').submit();"/></a>
              </div>
</div>

<div class="row">
<div class="col-md-12">

  <div class="table-responsive">
<br>
    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Setor Terakhir</th>
          <th>Tgl. Akhir Bayar</th>
          <th>Tgl. Jatuh Tempo</th>
          <th>Nama Anggota</th>
          <th>No.Angota</th>
          <th>Status</th>
          <th>Status JT</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $belum = $alertbulan-1;?>
        @foreach($simpanan as $simpananas)
        @if($datas = App\Simpananadmin::where('no_anggota',$simpananas->no_anggota)->where('jenis_simpanan',2)->where('aktif',1)->whereBetWeen('tgl_setor',[$alertbulan.'-01-01',$alertbulan.'-12-31'])->orderBy('id','ASC')->first())
        @if($stts =='Semua' || $stts == 'Sudah Bayar')
        <tr>
          <td>{{$no++}}.</td>
          <td>Rp {{number_format($datas->nominal,0,",",".")}}</td>
          <td>{{date('d-m-Y', strtotime($datas->tgl_setor))}}</td>
          <td>{{date('d-m-', strtotime($simpananas->jth_tempo))}}{{$alertbulan+1}}</td>
          <td>{{$datas->name}}</td>
          <td>{{$datas->no_anggota}}</td>
          <td><span class="label label-info-border">Sudah Bayar</span></td>
          <td><span class="label label-warning-border">OK</span> </td>
        </tr>
        @endif
        @elseif($datas = App\Simpananadmin::where('no_anggota',$simpananas->no_anggota)->where('jenis_simpanan',2)->where('aktif',1)->whereBetWeen('tgl_setor',[$belum.'-01-01',$belum.'-12-31'])->orderBy('id','ASC')->first())
        @if($stts == 'Semua' || $stts == 'Belum Bayar')
        <tr>
          <td>{{$no++}}.</td>
          <td>Rp {{number_format($datas->nominal,0,",",".")}}</td>
          <td>{{date('d-m-Y', strtotime($datas->tgl_setor))}}</td>
          <td>{{date('d-m-', strtotime($simpananas->jth_tempo))}}{{$alertbulan}}</td>
          <td>{{$datas->name}}</td>
          <td>{{$datas->no_anggota}}</td>
          <td><span class="label label-danger-border">Belum Bayar</span></td>
          <td><?php $jumlah = (int)$alertbulan.date('md', strtotime($simpananas->jth_tempo)); ?>
            @if($jumlah < date('Ymd'))<span class="label label-danger-border">Sudah Jatuh Tempo</span> @else <span class="label label-success-border">Belum Jatuh Tempo</span> @endif</td>
        </tr>
        @endif
        @endif
        @endforeach
        @if(count($simpanan) < 1)
        <tr>
          <td colspan="6" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<!-- <form class="" action="{{url('/administrator/data-simpanan')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-2"><br><br>
      <input class="form-control rounded btn-success" type="submit" value="Upload">
  </div>
</form> -->
</div>

    </div>


      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
