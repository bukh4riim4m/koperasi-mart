@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Pemesanan</h4>
</div>

</div>
<div class="row">
<div class="col-md-4">
  <div class="profile-widget">
    <div class="profile-imges">
      <img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%">
    </div>

    <br>
    <div class="profile-imges">
      <h4>Detail Barang</h4><!-- <a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a> -->
    </div><br>
    <p><h6 class="user-name m-t-10 m-b-0 text-left">Kode : {{$produk->kode}}</h6></p>
    <h6 class="user-name m-t-10 m-b-0 text-left">Nama : {{$produk->name}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Harga : Rp {{number_format($produk->harga,0,",",".")}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Berat : {{$produk->berat}} Gram / {{$produk->berat/1000}} Kg</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Stok : {{$produk->stok}}</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Keterangan : {{$produk->keterangan}}</h6>
    <div class="small text-muted"></div>
  </div>
</div>

<div class="col-md-8">
  <div class="profile-widget">

    <form class="" action="{{url('/administrator/bayar')}}" method="post" id="beli">
      @csrf
      <div class="profile-imges">
        <h4>Pemesanan</h4><!-- <a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a> -->
      </div><br>
    <h6 class="user-name m-t-10 m-b-0 text-left">Jumlah :

    <select name="jumlah" id="jumlah">
      <?php for ($i=1; $i < $produk->stok+1; $i++) {
    echo "<option value=".$i.">".$i."</option>";
} ?>
    </select> </h6>


    <input type="hidden" name="ids" id="ids" value="{{$produk->id}}">

    <script type="text/javascript">

                $("select[name='jumlah']").change(function(){
                  $(document).ajaxStart(function(){
                      $("#wait").css("display", "block");
                  });
                  $(document).ajaxComplete(function(){
                      $("#wait").css("display", "none");
                  });
                  $("button").click(function(){
                      $("#txt").load("demo_ajax_load.asp");
                  });
                  // alert('change');
                    var jumlah = $(this).val();
                    var ids = $("#ids").val();
                    var token = $("input[name='_token']").val();
                    $.ajax({
                        url: "<?php echo route('select-jumlah') ?>",
                        method: 'POST',
                        data: {ids:ids,jumlah:jumlah, _token:token},
                        success: function(data) {
                          console.log(data);
                          $("#id_harga").html("");
                          $("#city").html("");
                          $("#id_harga").append(data);
                          $("#divharga").hide();
                          $("#send").hide();

                        }
                    });
                });
              </script>
    <h6 class="user-name m-t-10 m-b-0 text-left">Total Harga : <label name="id_harga" id="id_harga">Rp. {{number_format($produk->harga,0,",",".")}}</label></h6><hr>
    <h6 class="user-name m-t-10 m-b-0 text-left">Kirim Dari : Cilegon</h6>
    <h6 class="user-name m-t-10 m-b-0 text-left">Kirim Ke : <br><br>
      <select style="width:100%;" name="province" id="province" required>
        <option value="">Pilih Propinsi</option>
        <?php foreach ($detail as $key => $value) {
    echo "<option value=".$value['province_id'].">".$value['province']."</option>";
};?>
      </select>

<br>
<center> <div id="wait" style="display:none;width:50%;border:0px solid black;position:absolute;top:50%;padding:5px;"><img src="{{url('images/load.gif')}}" width="100%" /></div></center>
<input type="hidden" name="berat" value="{{$produk->berat}}" id="berat">
<script type="text/javascript">

            $("select[name='province']").change(function(){
              $(document).ajaxStart(function(){
                  $("#wait").css("display", "block");
              });
              $(document).ajaxComplete(function(){
                  $("#wait").css("display", "none");
              });
              $("button").click(function(){
                  $("#txt").load("demo_ajax_load.asp");
              });
              // alert('change');
                var province = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('select-province') ?>",
                    method: 'POST',
                    data: {province:province, _token:token},
                    success: function(data) {
                      console.log(data);
                      $("#city").html("");
                      $("#city").append(data);
                      $("#divharga").hide();
                      $("#send").hide();
                    }
                });
            });
          </script>
    <br>
      <select style="width:100%;" name="city" id="city" required>
        <option value="">Pilih Kota</option>
      </select>
      <script type="text/javascript">

                  $("select[name='city']").change(function(){
                    $(document).ajaxStart(function(){
                        $("#wait").css("display", "block");
                    });
                    $(document).ajaxComplete(function(){
                        $("#wait").css("display", "none");
                    });
                    $("button").click(function(){
                        $("#txt").load("demo_ajax_load.asp");
                    });
                    // alert('change');
                      var city = $(this).val();
                      var jumlah = $("#jumlah").val();
                      var berat = $("#berat").val();
                      var token = $("input[name='_token']").val();
                      $.ajax({
                          url: "<?php echo route('select-city') ?>",
                          method: 'POST',
                          data: {jumlah:jumlah,berat:berat,city:city, _token:token},
                          success: function(data) {
                            console.log('success');
                            console.log(data);
                            $("#detail").html("");
                            $("#detail").append(data);
                            $("#divharga").show();
                            $("#send").show();
                          }
                      });
                  });
                </script>
      <br><br>
      <textarea name="alamat" rows="3" style="width:100%;" value="" placeholder="Alamat Lengkap (Jln, RT RW, Telp Pengirim & Penerima)" required></textarea>
    </h6><br>
    <h6><div id="send">
      <select class="" name="kirim" id="kirim"  style="width:100%;">
        <option value="jne">Melalui : JNE</option>
        <option value="tiki">Melalui : TIKI</option>
        <option value="pos">Melalui : POS</option>
      </select>
    </div></h6>

    <script>
      $("#send").hide();
    </script>
    <script type="text/javascript">

                $("select[name='kirim']").change(function(){
                  $(document).ajaxStart(function(){
                      $("#wait").css("display", "block");
                  });
                  $(document).ajaxComplete(function(){
                      $("#wait").css("display", "none");
                  });
                  $("button").click(function(){
                      $("#txt").load("demo_ajax_load.asp");
                  });
                    var kirim = $(this).val();
                    var city = $("#city").val();
                    var jumlah = $("#jumlah").val();
                    var berat = $("#berat").val();
                    var token = $("input[name='_token']").val();
                    $.ajax({
                        url: "<?php echo route('select-kirim') ?>",
                        method: 'POST',
                        data: {kirim:kirim,jumlah:jumlah,berat:berat,city:city, _token:token},
                        success: function(data) {
                          console.log('success');
                          console.log(data);
                          $("#detail").html("");
                          $("#detail").append(data);
                          $("#divharga").show();
                          $("#send").show();
                        }
                    });
                });
              </script>

    <div id="divharga">
      <h6 class="text-left" id="detail" name="detail">DETAIL</h6>
    </div>
    <script>
      $("#divharga").hide();
    </script>
      <hr>
      <input type="hidden" name="action" value="beli">
      <input type="hidden" name="ids" value="{{$produk->id}}">
      <a href="#" onclick="event.preventDefault();
                    document.getElementById('beli').submit();" class="btn btn-primary btn-sm m-t-10">PEMBAYARAN</a>
      <a href="#" data-toggle="modal" data-target="#detail{{$produk->id}}" class="btn btn-default btn-sm m-t-10">BATAL</a>
    </form>
  </div>
</div>

</div>
</div>
</div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
