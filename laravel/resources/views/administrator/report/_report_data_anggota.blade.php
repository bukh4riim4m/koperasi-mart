<tbody>
  <tr>
    <td>No</td>
    <td>No-KSSD</td>
    <td>Nama</td>
    <td>No-KS212</td>
    <td>No. KTP</td>
    <td>Email</td>
    <td>Nomor Telpon</td>
    <td>Tempat Lahir</td>
    <td>Tanggal Lahir</td>
    <td>NPWP</td>
    <td>Agama</td>
    <td>Pendidikan Terakhir</td>
    <td>Status Dalam Keluarga</td>
    <td>Nama Ibu</td>
    <td>Nama Ayah</td>
    <td>Jenis Kelamin</td>
    <td>Alamat Anggota</td>
    <td>Kelurahan</td>
    <td>Kecamatan</td>
    <td>Kabupaten</td>
    <td>Propinsi</td>
    <td>Komunitas</td>
    <td>Pendapatan</td>
    <td>Pekerjaan</td>
    <td>Saldo</td>
  </tr>
  <?php $no=1;?>
  @foreach($user as $use)
  <tr>
    <td>{{$no++}}</td>
    <td>{{$use->no_anggota}}</td>
    <td>{{$use->name}}</td>
    <td>{{$use->no_ks212}}</td>
    <td>{{$use->nik}}</td>
    <td>{{$use->email}}</td>
    <td>{{$use->telp}}</td>
    <td>{{$use->tpt_lahir}}</td>
    <td>{{date('d-m-Y', strtotime($use->tgl_lahir))}}</td>
    <td>{{$use->npwp}}</td>
    <td>{{$use->agama}}</td>
    <td>@if($use->pendidikan > 0){{$use->pendidikan_id->name}} @else  @endif</td>
    <td>{{$use->statuskeluarga}}</td>
    <td>{{$use->nama_ibu}}</td>
    <td>{{$use->nama_ayah}}</td>
    <td>{{$use->jenkel}}</td>
    <td>{{$use->alamat}}</td>
    <td>@if($use->kelurahan > 0){{ $use->kelurahan_id->name }} @else  @endif</td>
    <td>@if($use->kecamatan > 0){{ $use->kecamatan_id->name }} @else  @endif</td>
    <td>@if($use->kabupaten > 0){{ $use->kabupaten_id->name }} @else  @endif</td>
    <td>@if($use->propinsi > 0){{ $use->propinsi_id->name }} @else @endif</td>
    <td>@if($use->komunitas > 0){{ $use->komunitas_id->name }} @else @endif</td>
    <td>@if($use->pendapatan > 0){{ $use->pendapatan_id->name }} @else @endif</td>
    <td>@if($use->pekerjaan > 0){{ $use->pekerjaan_id->name }} @else @endif</td>
    <td>{{ $use->saldo }}</td>
  </tr>
  @endforeach
  <tr>
    <th colspan="3">Total</td>
    <td></td>
  </tr>
</tbody>
