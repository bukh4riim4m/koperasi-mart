<table class="table table-striped custom-table">
  <thead>
    <tr>
      <th>No.</th>
      <th>Setor Terakhir</th>
      <th>Tgl. Akhir Bayar</th>
      <th>Tgl. Jatuh Tempo</th>
      <th>Nama Anggota</th>
      <th>No.Angota</th>
      <th>Status</th>
      <th>Status JT</th>
    </tr>
  </thead>
  <tbody>
    <?php $no=1;
    $belum = $alertbulan-1;?>
    @foreach($simpanan as $simpananas)
    @if($datas = App\Simpananadmin::where('no_anggota',$simpananas->no_anggota)->where('jenis_simpanan',2)->where('aktif',1)->whereBetWeen('tgl_setor',[$alertbulan.'-01-01',$alertbulan.'-12-31'])->orderBy('id','ASC')->first())
    @if($stts =='Semua' || $stts == 'Sudah Bayar')
    <tr>
      <td>{{$no++}}.</td>
      <td>{{$datas->nominal}}</td>
      <td>{{$datas->tgl_setor}}</td>
      <td>{{$alertbulan+1}}{{date('-m-d', strtotime($simpananas->jth_tempo))}}</td>
      <td>{{$datas->name}}</td>
      <td>{{$datas->no_anggota}}</td>
      <td><span class="label label-info-border">Sudah Bayar</span></td>
      <td><span class="label label-warning-border">OK</span> </td>
    </tr>
    @endif
    @elseif($datas = App\Simpananadmin::where('no_anggota',$simpananas->no_anggota)->where('jenis_simpanan',2)->where('aktif',1)->whereBetWeen('tgl_setor',[$belum.'-01-01',$belum.'-12-31'])->orderBy('id','ASC')->first())
    @if($stts == 'Semua' || $stts == 'Belum Bayar')
    <tr>
      <td>{{$no++}}.</td>
      <td>{{$datas->nominal}}</td>
      <td>{{$datas->tgl_setor}}</td>
      <td>{{$alertbulan}}{{date('-m-d', strtotime($simpananas->jth_tempo))}}</td>
      <td>{{$datas->name}}</td>
      <td>{{$datas->no_anggota}}</td>
      <td><span class="label label-danger-border">Belum Bayar</span></td>
      <td><?php $jumlah = (int)$alertbulan.date('md', strtotime($simpananas->jth_tempo)); ?>
        @if($jumlah < date('Ymd'))<span class="label label-danger-border">Sudah Jatuh Tempo</span> @else <span class="label label-success-border">Belum Jatuh Tempo</span> @endif</td>
    </tr>
    @endif
    @endif
    @endforeach
    @if(count($simpanan) < 1)
    <tr>
      <td colspan="6" class="text-center">KOSONG</td>
    </tr>
    @endif
  </tbody>
</table>
