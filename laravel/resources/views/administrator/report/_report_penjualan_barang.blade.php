<thead>
  <tr>
    <th>No</th>
    <th>No.Anggota</th>
    <th>No. Pemesanan</th>
    <th>Tgl. Pemesanan</th>
    <th>Kode barang</th>
    <th>Nama Barang</th>
    <th>Berat</th>
    <th>Ongkir</th>
    <th>Jumlah</th>
    <th>Harga</th>
    <th>Melalui</th>
    <th>Alamat Kirim</th>
    <th>Status</th>
  </tr>
  <?php $no=1;
  $total=0;?>
  </thead>
  <tbody>
  @foreach($datas as $data)
  <?php $total+= $data->nominal;?>

  <tr>
    <td>{{$no++}}</td>
    <td>{{$data->no_anggota}}</td>
    <td>{{$data->no_pemesanan}}</td>
    <td>{{$data->tanggal}}</td>
    <td>{{$data->kode}}</td>
    <td>{{$data->tokoId->name}}</td>
    <td>{{$data->berat}} Gram</td>
    <td>{{$data->ongkir}}</td>
    <td>{{$data->jumlah}}</td>
    <td>{{$data->total_harga - $data->ongkir}}</td>
    <td>{{$data->melalui}}</td>
    <td>{{$data->alamat}}</td>
    <td>{{$data->statusId->name}}</td>
  </tr>
  @endforeach

</tbody>
