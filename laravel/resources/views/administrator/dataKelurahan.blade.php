@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-6">
		<h4 class="page-title">Data Kelurahan</h4>
	</div>
  <div class="col-xs-6">
    <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i> Tambah Kelurahan</a>
  </div>
</div>

<div class="row filter-row">
  <form class="" action="{{url('/administrator/data-kelurahan')}}" method="post">
    @csrf
<input type="hidden" name="action" value="cari">
	<div class="col-sm-6 col-xs-6">
		<div class="form-group form-focus">
			<label class="control-label">Kelurahan</label>
			<input type="text" name="name" value="" class="form-control floating">
		</div>
	</div>
	<div class="col-sm-6 col-xs-6">
		<!-- <a href="#" class="btn btn-success btn-block"> Search </a> -->
    <input type="submit" class="btn btn-success btn-block" name="btn" value="Search">
	</div>
</form>
</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped custom-table datatable">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
						<th>Kelurahan</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
          <?php $no=1; ?>
          @foreach($kelurahans as $kel)
					<tr class="holiday-completed">
						<td>{{$no++}}.</td>
						<td>{{$kel->name}}</td>
						<td style="min-width:150px;">
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_komunitas{{$kel->id}}">Edit</a>
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_komunitas{{$kel->id}}">Hapus</a>
						</td>
					</tr>
          @endforeach
				</tbody>

			</table>
		</div>
	</div>
</div>

<div id="add_golongan_pegawai" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Form Kelurahan</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/administrator/data-kelurahan')}}" method="post">
          <input type="hidden" name="action" value="tambah">
            @csrf
          <div class="form-group">
            <label>Nama Kelurahan <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="name">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@foreach($kelurahans as $kelu)
<div id="edit_komunitas{{$kelu->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Edit Kelurahan</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/administrator/data-kelurahan/'.$kelu->id)}}" method="post">
          @csrf
          <div class="form-group">
            <label>Nama Kelurahan <span class="text-danger">*</span></label>
            <input class="input-sm form-control" name="name" value="{{$kelu->name}}" type="text" required>
            <input type="hidden" name="action" value="edit">
          </div>
          <div class="m-t-20 text-center">
            <button class="btn btn-success">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>



<div id="hapus_komunitas{{$kelu->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Kelurahan</h4>
      </div>
      <form action="{{url('/administrator/data-kelurahan/'.$kelu->id)}}" method="post" id="hapus_golongan">
        <input type="hidden" name="action" value="hapus">
        @csrf
        <div class="modal-body card-box">
          <p>Apakah yakin ingin di Hapus : {{$kelu->name}}</p>
          <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Kembali</a>
            <button type="submit" class="btn btn-danger">Hapus</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach
  </div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
