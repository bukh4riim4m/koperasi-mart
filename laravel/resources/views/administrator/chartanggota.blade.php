@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center">

              <?php

              $lakilaki = (int)$lakis[0]->totalLaki;
              $perempuans = (int)$perempuan[0]->totalPerempuan;
              $semuas = (int)$perempuans+$lakilaki;
              $persenlaki = $lakilaki/$semuas*100;
              $persenperempuans = $perempuans/$semuas*100;
                $dataPoints = array(
                	array("label"=>"Laki-Laki", "y"=>$persenlaki),
                	array("label"=>"Perempuan", "y"=>$persenperempuans),
                )

                ?>
                <script>
                window.onload = function() {


                var chart = new CanvasJS.Chart("chartContainer", {
                	animationEnabled: true,
                	title: {
                		text: "Statistik Jenis Kelamin"
                	},
                	subtitles: [{
                		text: "Total Anggota : {{$semuas}}"
                	}],
                	data: [{
                		type: "pie",
                		yValueFormatString: "#,##0.00\"%\"",
                		indexLabel: "{label} ({y})",
                		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                	}]
                });
                chart.render();

                }
                </script>
                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>
@endsection
