@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Data Karyawan</h4>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{('/administrator/data-karyawan')}}" method="post">
    @csrf
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No.Karyawan</label>
        <input type="text" class="form-control floating" name="no_karyawan" value="{{$nomor}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Karyawan</label>
        <input type="text" class="form-control floating" name="name" value="{{$name}}"/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">
    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Foto</th>
          <th>No.Karyawan</th>
          <th>Nama Karyawan</th>
          <th>No Telp</th>
          <th>Tgl Lahir</th>
          <th>ID Login</th>
          <th class="text-right"><a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#tambah">Tambah</a></th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        @foreach($user as $us)
        <tr>
          <td>{{$no++}}.</td>
          <td><img class="avatar" src="{{ url('/laravel/public/foto/'.$us->fotodiri) }}" alt=""></td>
          <td>{{$us->no_anggota}}</td>
          <td>{{$us->name}}</td>
          <td>{{$us->telp}}</td>
          <td>{{date('d-m-Y', strtotime($us->tgl_lahir))}}</td>
          <td>{{$us->sequence}}</td>
          <td style="max-width:70px;" class="text-right">
            <a href="editinfaq{{$us->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit{{$us->id}}">Edit</a>
            <a href="hapusinfaq{{$us->id}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#delete{{$us->id}}">Hapus</a>
          </td>
        </tr>
        @endforeach
        @if(count($user) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>
</div>
    </div>


    <div id="tambah" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Tambah Karyawan</h4>
          </div>

          <div class="modal-body">
            <form class="m-b-30" action="{{url('/administrator/data-karyawan')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="action" value="tambah">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No. Karyawan<span class="text-danger">* [min-6]</span></label>
                    <input class="form-control" type="number" name="no_karyawan" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir"></div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email <span class="text-danger">* </span></label>
                    <input class="form-control" type="email" name="email" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="telp" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                    <select class="select" name="jenkel" required>
                      <option value="">Pilih Jenis Kelamin</option>
                      <option value="Laki-laki">LAKI-LAKI</option>
                      <option value="Perempuan">PEREMPUAN</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">ID Login <span class="text-danger">* [min-6]</span></label>
                    <input class="form-control" type="number" name="id_login" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Password Login <span class="text-danger">* [min-6]</span></label>
                    <input class="form-control" type="password" name="password_login" required>
                  </div>
                </div>
              </div>

              <div class="m-t-20 text-center">
                <button class="btn btn-primary">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- DETAIL -->
    @foreach($user as $use)
    <div id="detail{{$use->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Detail Data Pengurus</h4>
      </div>
      <div class="modal-body">
          <table class="table">
            <tr>
              <td colspan="2" align="center"><img src="{{url('/laravel/public/ktp/'.$use->fotoktp)}}" width="150px"/></td>
            </tr>
            <tr>
              <td>Nomor Anggota</td>
              <td>: {{$use->no_anggota}}</td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>: {{$use->name}}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>: {{$use->email}}</td>
            </tr>
            <tr>
              <td>Nomor Telpon</td>
              <td>: {{$use->telp}}</td>
            </tr>
            <tr>
              <td>No.NPWP</td>
              <td>: {{$use->npwp}}</td>
            </tr>
            <tr>
              <td>Tempat Lahir</td>
              <td>: {{$use->tpt_lahir}}</td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td>
              <td>: {{date('d-m-Y', strtotime($use->tgl_lahir))}}</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>: {{$use->jenkel}}</td>
            </tr>
            <tr>
              <td>Agama</td>
              <td>: {{$use->agama}}</td>
            </tr>
            <tr>
              <td>Pendidikan</td>
              <td>: {{$use->pendidikan}}</td>
            </tr>
            <tr>
              <td>Status dalam keluarga</td>
              <td>: {{$use->statuskeluarga}}</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>: {{$use->alamat}}</td>
            </tr>
            <tr>
              <td colspan="2"></td>
            </tr>
          </table>
      </div>
    </div>
  </div>
</div>
<!-- END DETAIL -->

<!-- EDIT -->
    <div id="edit{{$use->id}}" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Karyawan</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/administrator/data-karyawan')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="action" value="edit">
              <input type="hidden" name="ids" value="{{$use->id}}">
              @csrf
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="{{$use->name}}" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Karyawan <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="no_karyawan" value="{{$use->no_anggota}}" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="telp" value="{{$use->telp}}" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value="{{date('d-m-Y', strtotime($use->tgl_lahir))}}"></div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                    <?php $jns_klm = [
                      'Laki-Laki',
                      'Perempuan',
                    ]; ?>
                    <select class="select" name="jenkel" required>
                      <option value="">Pilih Jenis Kelamin</option>
                      @foreach($jns_klm as $jns)
                      @if($jns == $use->jenkel)
                      <option value="{{$jns}}" selected>{{$jns}}</option>
                      @else
                      <option value="{{$jns}}">{{$jns}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto <span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="fotodiri">
                  </div>
                </div>
              </div>
              <div class="m-t-20 text-center">
                <button class="btn btn-primary">Save Changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
<!-- END EDIT -->
<!-- DELETE -->
    <div id="delete{{$use->id}}" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Hapus Data Karyawan</h4>
          </div>
          <form action="{{url('/administrator/data-karyawan')}}" method="post">
            @csrf
            <input type="hidden" name="action" value="hapus">
            <input type="hidden" name="ids" value="{{$use->id}}">
            <div class="modal-body card-box">
              <p>Apakah yakin ingin di Hapus "{{$use->name}}" ???</p>
              <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-danger">Delete</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    @endforeach
    <!-- END DELETE -->
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
