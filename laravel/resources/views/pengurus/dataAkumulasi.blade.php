@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Akumulasi Belanja</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_akumulasi"><i class="fa fa-plus"></i> Tambah Akumulasi</a> -->
  <div class="view-icons">
  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No.KSSD</label>
        <input type="text" class="form-control floating" name="no_kssd" value="{{$no_kssd}}" minlength="6"/>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="to" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{url('/pengurus/akumulasi-belanja')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="action" value="export">
    <input type="hidden" name="no_kssd" value="{{$no_kssd}}"/>
    <input type="hidden" name="from" value="{{$from}}"/>
    <input type="hidden" name="to" value="{{$to}}"/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.KSSD</th>
          <th>No.Transaksi</th>
          <th>Tgl. Transaksi</th>
          <th>Nominal</th>
          <th>Gerai</th>
          <th>Type</th>
          <!-- <th style="max-width:110px;" class="text-center">Action</th> -->
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $total=0;?>
        @foreach($akumulasis as $akumulasi)
        <?php $total+= $akumulasi->nominal;?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$akumulasi->no_kssd}}</td>
          <td>{{$akumulasi->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($akumulasi->tgl_trx))}}</td>
          <td>Rp {{number_format($akumulasi->nominal,0,",",".")}}</td>
          <td>{{$akumulasi->gerai}}</td>
          <td>@if($akumulasi->type =='Non')Non Anggota @else {{$akumulasi->type}} @endif</td>
        </tr>
        @endforeach
        @if(count($akumulasis) < 1)
        <tr>
          <td colspan="7" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Akumulasi</td>
    <td>: Rp {{number_format($total,0,",",".")}}</td>
  </tr>
</table>
</div>
<div class="col-md-5">
  <!-- <a href="{{url('/pengurus/akumulasi-belanja')}}" class="pull-right"><img width="50px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export').submit();"/></a> -->
</div>
</div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>


@endsection
