@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Data Group</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Pengurus</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{('/pengurus/data-group')}}" method="post">
    @csrf
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value="{{$nomor}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Anggota</label>
        <input type="text" class="form-control floating" name="name" value="{{$name}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-md-3 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Hak Akses</label>
        <?php $jenis = ['Admin','Pengurus','Anggota'] ?>
        <select class="select floating" name="type">
          <option value=""> -- Semua -- </option>
          @foreach($jenis as $jen)
            @if($type == $jen)
              <option value="{{$jen}}" selected> {{$jen}} </option>
              @else
            <option value="{{$jen}}"> {{$jen}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div><a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a>
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table datatable">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Angota</th>
          <th>Nama</th>
          <th class="text-left">Hak Akses</th>
          <th class="text-right">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        @foreach($user as $us)
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$us->no_anggota}}</td>
          <td>{{$us->name}}</td>
          <td class="text-left">
							<div class="dropdown action-label">
								<a class="btn btn-white btn-sm rounded dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                  @if($us->type=='admin')
									<i class="fa fa-dot-circle-o text-danger"></i> {{$us->type}}
                  @elseif($us->type=='pengurus')
                  <i class="fa fa-dot-circle-o text-success"></i> {{$us->type}}
                  @else
                  <i class="fa fa-dot-circle-o text-info"></i> {{$us->type}}
                  @endif
								</a>
							</div>
						</td>
            <td class="text-right" align="right">@if($us->fotodiri !='')
              <a href="{{url('/pengurus/download-foto/'.$us->id)}}" class="btn btn-primary">Download Foto</a>@endif
            </td>
        </tr>
        @endforeach
        @if(count($user) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>
</div>
    </div>

      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
