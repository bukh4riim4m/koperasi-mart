@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-6">
		<h4 class="page-title">Saldo Akumulasi</h4>
	</div>
  <div class="col-xs-6">
    <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i> Tambah jenissimpanan</a> -->
  </div>
</div>

<div class="row filter-row">
  <form class="" action="{{url('/pengurus/saldo-akumulasi-belanja')}}" method="post">
    @csrf
<input type="hidden" name="action" value="cari">
	<div class="col-sm-4 col-xs-6">
		<div class="form-group form-focus">
			<label class="control-label">Nomor Anggota</label>
			<input type="text" name="no_anggota" value="{{$nomor}}" class="form-control floating">
		</div>
	</div>
  <div class="col-sm-3 col-md-4 col-xs-6">
    <div class="form-group form-focus select-focus">
      <label class="control-label">Tahun</label>
      <?php $tahuns = App\Tahun::get(); ?>
      <select class="select floating" name="tahun">
        @foreach($tahuns as $thn)
          @if($tahun == $thn->name)
            <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
          @else
            <option value="{{$thn->name}}"> {{$thn->name}} </option>
          @endif
        @endforeach
      </select>
    </div>
  </div>
	<div class="col-sm-4 col-xs-12">
		<!-- <a href="#" class="btn btn-success btn-block"> Search </a> -->
    <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN">
	</div>
</form>
</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
    @if($nomor)
    <div class="card-box m-b-0">
    <div class="row">
      <div class="col-md-12">
        <div class="profile-view">
          <div class="profile-img-wrap">
            <div class="profile-img">
              <a href=""><img class="avatar" src="{{ url('/laravel/public/foto/'.$users->fotodiri) }}" alt=""></a>
            </div>
          </div>
          <div class="profile-basic">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <td>Nomor Anggota<span class="pull-right">:</span></td>
                      <td>@if($users->no_anggota==null) @else {{$users->no_anggota}} @endif</td>
                    </tr>
                    <tr>
                      <td>Nama Lengkap<span class="pull-right">:</span></td>
                      <td>@if($users->name==null) @else {{$users->name}} @endif</td>
                    </tr>
                    <tr>
                      <td>Nomor Telpon<span class="pull-right">:</span></td>
                      <td>@if($users->telp==null) @else {{$users->telp}} @endif</td>
                    </tr>
                    <tr>
                      <td>Periode Tahun<span class="pull-right">:</span></td>
                      <td>01-Jan-{{$tahun}} s/d 31-Des-{{$tahun}}</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

		<div class="table-responsive"><hr>
			<table class="table table-striped custom-table">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
            <th>No.Anggota</th>
						<th>Tanggal Transaksi</th>
            <th>No. Transaksi</th>
            <th>Nominal</th>
            <th>Akumulasi</th>
            <th>Gerai</th>
					</tr>
				</thead>
				<tbody><?php $no=1; $saldo =0;?>
          @foreach($datas as $data)
					<tr class="holiday-completed">
            <?php $saldo+= $data->nominal;?>
						<td>{{$no++}}</td>
						<td>{{$data->no_kssd}}</td>
            <td>{{$data->tgl_trx}}</td>
						<td>{{$data->no_trx}}</td>
            <td>Rp {{number_format($data->nominal,0,",",".")}}</td>
            <td>Rp {{number_format($saldo,0,",",".")}}</td>
						<td>{{$data->gerai}}</td>
					</tr>
          @endforeach
				</tbody>

			</table>
		</div>
    <div class="row col-md-6 table-responsive">
      <table class="table table-striped custom-table" width="40px">
        <thead>
          <tr>
            <th>Total Akumulasi</th>
            <th>: Rp {{number_format($saldo,0,",",".")}}</th>
          </tr>
        </thead>
      </table>
    </div>

	</div>
</div>

@endif
  </div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
